<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-wóz | Strona główna</title>
    <meta name="description" content="Złotówa - twoje miejsce z samochodami">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- Body content -->

    <jsp:include page="/menu" />

    <div class="slider-area">
        <div class="slider">
            <div id="bg-slider" class="owl-carousel owl-theme">
                <div class="item"><img style="object-fit: cover" src="/resources/assets/img/slide1/slider-image-4.jpg" alt=""></div>
                <div class="item"><img style="object-fit: cover" src="/resources/assets/img/slide1/slider-image-2.jpg" alt=""></div>
                <div class="item"><img style="object-fit: cover" src="/resources/assets/img/slide1/slider-image-1.jpg" alt=""></div>
            </div>
        </div>
        <div class="container slider-content">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-10 col-md-offset-1 col-sm-12" style="background-color: #fcfcfc">
                    <h2>Szybkie samochody w zasięgu ręki!</h2>
                    <p>Kupuj, nie zwlekaj aż ktoś Cię ubiegnie!</p>
                </div>
            </div>
        </div>
    </div>

    <%--<div class="home-lager-shearch" style="background-color: rgb(252, 252, 252); padding-top: 25px; margin-top: -125px;">--%>
        <%--<div class="container">--%>
            <%--<div class="col-md-12 large-search">--%>
                <%--<div class="search-form wow pulse">--%>
                    <%--<form action="" class=" form-inline">--%>
                        <%--<div class="col-md-12">--%>
                            <%--<div class="col-md-4">--%>
                                <%--<input type="text" class="form-control" placeholder="Słowo kluczowe">--%>
                            <%--</div>--%>
                            <%--<div class="col-md-2">--%>
                                <%--<select id="car-make" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Wybierz markę">--%>
                                    <%--<option>Opel</option>--%>
                                    <%--<option>Volkswagen</option>--%>
                                    <%--<option>Audi</option>--%>
                                    <%--<option>Toyota</option>--%>
                                    <%--<option>Ford</option>--%>
                                    <%--<option>Lexus</option>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-2">--%>
                                <%--<select id="gearbox" class="selectpicker show-tick form-control" title="Rodzaj skrzyni">--%>
                                    <%--<option>Ręczna</option>--%>
                                    <%--<option>Automatyczna</option>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-2">--%>
                                <%--<select id="state" class="selectpicker show-tick form-control" title="Wybierz stan">--%>
                                    <%--<option>Nowy</option>--%>
                                    <%--<option>Używany</option>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                            <%--<div class="col-md-2">--%>
                                <%--<select id="gas-type" class="selectpicker show-tick form-control" title="Wybierz paliwo">--%>
                                    <%--<option>Benzyna</option>--%>
                                    <%--<option>Diesel</option>--%>
                                <%--</select>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                        <%--<div class="col-md-12 ">--%>
                            <%--<div class="search-row">--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<label for="price-range">Zakres cenowy (PLN):</label>--%>
                                    <%--<input type="text" class="span2" value="" data-slider-min="2000"--%>
                                           <%--data-slider-max="50000" data-slider-step="500"--%>
                                           <%--data-slider-value="[7000,14000]" id="price-range" ><br />--%>
                                    <%--<b class="pull-left color">2000 PLN</b>--%>
                                    <%--<b class="pull-right color">50000 PLN</b>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<label for="production-date">Rok produkcji:</label>--%>
                                    <%--<input type="text" class="span2" value="" data-slider-min="1990"--%>
                                           <%--data-slider-max="2017" data-slider-step="1"--%>
                                           <%--data-slider-value="[1995,2000]" id="production-date" ><br />--%>
                                    <%--<b class="pull-left color">1990</b>--%>
                                    <%--<b class="pull-right color">2017</b>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<label for="mileage">Przebieg :</label>--%>
                                    <%--<input type="text" class="span2" value="" data-slider-min="0"--%>
                                           <%--data-slider-max="500000" data-slider-step="25000"--%>
                                           <%--data-slider-value="[0,100000]" id="mileage" ><br />--%>
                                    <%--<b class="pull-left color">0 km</b>--%>
                                    <%--<b class="pull-right color">500 000 km</b>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<label for="power">Moc silnika :</label>--%>
                                    <%--<input type="text" class="span2" value="" data-slider-min="50"--%>
                                           <%--data-slider-max="200" data-slider-step="10"--%>
                                           <%--data-slider-value="[70,100]" id="power" ><br />--%>
                                    <%--<b class="pull-left color">50 KM</b>--%>
                                    <%--<b class="pull-right color">200 KM</b>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                            <%--</div>--%>

                            <%--<div class="search-row">--%>

                                <%--<h3>Wygoda</h3>--%>
                                <%--<hr>--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> 4x4--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Czujnik parkowania--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Centralny zamek--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Elektryczne szyby--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Podgrzewane fotele--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Klimatyzacja--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Wspomaganie kierownicy--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Nawigacja GPS--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>
                            <%--</div>--%>
                            <%--<div class="search-row">--%>
                                <%--<h3>Bezpieczeństwo</h3>--%>
                                <%--<hr>--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> ABS--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> ASR--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Autoalarm--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Immobiliser--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Kontrola trakcji--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>

                                <%--<div class="col-sm-3">--%>
                                    <%--<div class="checkbox">--%>
                                        <%--<label>--%>
                                            <%--<input type="checkbox"> Poduszki powietrzne--%>
                                        <%--</label>--%>
                                    <%--</div>--%>
                                <%--</div>--%>
                                <%--<!-- End of  -->--%>
                            <%--</div>--%>
                        <%--</div>--%>
                        <%--<div class="center">--%>
                            <%--<button type="submit" value="" class="btn btn-default">--%>
                                <%--<i class="glyphicon glyphicon-search"></i> Szukaj--%>
                            <%--</button>--%>
                        <%--</div>--%>
                    <%--</form>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>


    <%--<!-- property area -->--%>
    <%--<div class="content-area recent-property" style="background-color: #FCFCFC; padding-bottom: 55px;">--%>
        <%--<div class="container">--%>
            <%--<div class="row">--%>
                <%--<div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">--%>
                    <%--<!-- /.feature title -->--%>
                    <%--<h2>Najczęściej oglądane</h2>--%>
                    <%--<p>Kup nim ktoś zrobi to za ciebie!</p>--%>
                <%--</div>--%>
            <%--</div>--%>

            <%--<div class="row">--%>
                <%--<div class="proerty-th">--%>
                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/1"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-1-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/1"/>" >VW Polo </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 2000 </span>--%>
                                <%--<span class="proerty-price pull-right">10 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/2"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-2-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/2"/>" >Audi A8 </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 2001 </span>--%>
                                <%--<span class="proerty-price pull-right">25 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/3"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-3-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/3"/>" >Toyota corrola </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 2004 </span>--%>
                                <%--<span class="proerty-price pull-right">15 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/4"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-4-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/4"/>" >VW Passat </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 2001 </span>--%>
                                <%--<span class="proerty-price pull-right">25 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>


                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/5"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-5-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/5"/>" >Opel Tigra </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 2001 </span>--%>
                                <%--<span class="proerty-price pull-right">25 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-two proerty-item">--%>
                            <%--<div class="item-thumb">--%>
                                <%--<a href="<spring:url value="/carDetails/6"/>" ><img style="object-fit: cover" src="<c:url value="/resources/assets/img/cars/id-6-zdj1.jpg"/>"></a>--%>
                            <%--</div>--%>
                            <%--<div class="item-entry overflow">--%>
                                <%--<h5><a href="<spring:url value="/carDetails/6"/>" >Polonez </a></h5>--%>
                                <%--<div class="dot-hr"></div>--%>
                                <%--<span class="pull-left"><b>Rocznik :</b> 1990 </span>--%>
                                <%--<span class="proerty-price pull-right">3 000 PLN</span>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <%--<div class="col-sm-6 col-md-3 p0">--%>
                        <%--<div class="box-tree more-proerty text-center">--%>
                            <%--<div class="item-tree-icon">--%>
                                <%--<i class="fa fa-th"></i>--%>
                            <%--</div>--%>
                            <%--<div class="more-entry overflow">--%>
                                <%--<h5><a href="property-1.html" >NIE MOŻESZ SIĘ ZDECYDOWAĆ?!</a></h5>--%>
                                <%--<h5 class="tree-sub-ttl">ZOBACZ RESZTĘ!!</h5>--%>
                                <%--<button class="btn border-btn more-black" value="Wszystkie pojazdy">Wszystkie pojazdy</button>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>

    <!-- Count area -->
    <div class="count-area">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 text-center page-title">
                    <!-- /.feature title -->
                    <h2>MOŻESZ NAM ZAUFAĆ!</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 percent-blocks m-main" data-waypoint-scroll="true">
                    <div class="row">
                        <div class="col-sm-4 col-xs-4">
                            <div class="count-item">
                                <div class="count-item-circle">
                                    <span class="pe-7s-users"></span>
                                </div>
                                <div class="chart" data-percent="5000">
                                    <h2 class="percent" id="counter">0</h2>
                                    <h5>ZADOWOLONYCH KLIENTÓW</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <div class="count-item">
                                <div class="count-item-circle">
                                    <span class="pe-7s-car"></span>
                                </div>
                                <div class="chart" data-percent="12000">
                                    <h2 class="percent" id="counter1">0</h2>
                                    <h5>DOSTĘPNYCH SAMOCHODÓW</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <div class="count-item">
                                <div class="count-item-circle">
                                    <span class="pe-7s-home"></span>
                                </div>
                                <div class="chart" data-percent="12000">
                                    <h2 class="percent" id="counter2">0</h2>
                                    <h5>PLACÓWEK NA TERENIE POLSKI</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="/footer" />


    <script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

    <script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/assets/js/bootstrap-select.min.js"></script>
    <script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

    <script src="/resources/assets/js/easypiechart.min.js"></script>
    <script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

    <script src="/resources/assets/js/owl.carousel.min.js"></script>

    <script src="/resources/assets/js/wow.js"></script>

    <script src="/resources/assets/js/icheck.min.js"></script>
    <script src="/resources/assets/js/price-range.js"></script>

    <script src="/resources/assets/js/main.js"></script>

</body>
</html>