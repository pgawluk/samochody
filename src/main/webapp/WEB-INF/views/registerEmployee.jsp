<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-Wóz | Rejestracja</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

    <jsp:include page="/menu" />

    <div class="page-head">
        <div class="container">
            <div class="row">
                <div class="page-head-content">
                    <h1 class="page-title text-center">Rejestracja Pracownika</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End page header -->


    <!-- register-area -->
    <div class="register-area" style="background-color: rgb(249, 249, 249);">
        <div class="container">
            <div class="col-md-6 col-md-offset-3">
                <div class="box-for overflow">
                    <div class="col-md-12 col-xs-12 register-blocks">
                        <br>
                        <form:form action="/employee/register" modelAttribute="employee" method="post">
                            <div class="form-group col-md-6">
                                <label for="firstname">Imię</label>
                                <form:input path="firstname" class="form-control" id="firstname" autofocus="autofocus" required="required" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="surname">Nazwisko</label>
                                <form:input path="lastname" class="form-control" id="surname" required="required" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="email">Adres e-mail</label>
                                <form:input path="email" type="email" class="form-control" id="email" required="required" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="password">Hasło</label>
                                <form:input path="password" type="password" class="form-control" id="password" required="required" />
                            </div>
                            <div class="form-group col-md-12">
                                <label for="confirm-password">Powtórz Hasło</label>
                                <input type="password" class="form-control" id="confirm-password" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="role"></label>
                                <form:select path="role" class="form-control" id="role">
                                    <form:option value="ROLE_MANAGER" >Kierownik</form:option>
                                    <form:option value="ROLE_ADMIN">Administrator</form:option>
                                </form:select>
                            </div>
                            <div class="text-center">
                                <form:button type="submit" class="btn btn-default">Zarejestruj Konto</form:button>
                            </div>
                        </form:form>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <jsp:include page="/footer" />


    <script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

    <script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/assets/js/bootstrap-select.min.js"></script>
    <script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

    <script src="/resources/assets/js/easypiechart.min.js"></script>
    <script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

    <script src="/resources/assets/js/owl.carousel.min.js"></script>

    <script src="/resources/assets/js/wow.js"></script>

    <script src="/resources/assets/js/icheck.min.js"></script>
    <script src="/resources/assets/js/price-range.js"></script>

    <script src="/resources/assets/js/main.js"></script>

</body>
</html>