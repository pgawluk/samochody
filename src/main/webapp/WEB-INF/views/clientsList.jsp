<%@ page import="mvc.models.UserModel" %>
<%@ page import="mvc.enums.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-wóz | Strona główna</title>
    <meta name="description" content="Złotówa - twoje miejsce z samochodami">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- Body content -->

<jsp:include page="/menu" />
<div class="page-head">
    <div class="container">
        <div class="row" style="text-align: center">
            <div class="page-head-content">
                <h1 class="page-title">Lista Klientów</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<!-- property area -->
<div class="content-area recent-property" style="background-color: #FFF;">
    <div class="container">
        <div class="row">

            <div class="col-md-12 pr-30 padding-top-40 properties-page user-properties">

                <div class="section">
                    <div id="list-type" class="proerty-th-list">

                        <jsp:useBean id="list" scope="request" type="java.util.List"/>
                        <c:forEach items="${list}" var="item">
                            <div class="col-md-12 "  style="padding-bottom: 20px">
                                <div class="box-two proerty-item">

                                    <div class="item-entry overflow">
                                        <h5><a href="property-1.html" style="padding-left: 15px">
                                            <c:out value="${item.firstname} ${item.lastname}"/>
                                        </a></h5>
                                        <div class="dot-hr"></div>
                                        <span class="pull-left" style="padding-left: 15px; padding-bottom: 8px">
                                       <c:out value="${item.email}"/>
                                    </span>
                                        <%
                                            UserModel userModel = (UserModel)session.getAttribute(("userFromSession"));
                                            if(userModel.getRole().equals(Role.ROLE_ADMIN)){%>

                                        <div class="dealer-action pull-right">
                                            <a href="/editAccount/${item.userId}" class="button">Edytuj </a>
                                            <a href="/deleteAccount/${item.userId}/${item.role}" class="button delete_user_car">Usuń</a>
                                            <a href="/setActiviti/${item.userId}" class="button">
                                                <%--<c:if test="${item.getIsActive}">--%>
                                                    <%--Zawieś--%>
                                                <%--</c:if>--%>
                                                Zawieś
                                            </a>
                                        </div>
                                        <%}%>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>

                    </div>

                </div>

                <%--<div class="section">--%>
                    <%--<div class="pull-right">--%>
                        <%--<div class="pagination">--%>
                            <%--<ul>--%>
                                <%--<li><a href="#">Prev</a></li>--%>
                                <%--<li><a href="#">1</a></li>--%>
                                <%--<li><a href="#">2</a></li>--%>
                                <%--<li><a href="#">3</a></li>--%>
                                <%--<li><a href="#">4</a></li>--%>
                                <%--<li><a href="#">Next</a></li>--%>
                            <%--</ul>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>

            </div>

        </div>
    </div>
</div>

<jsp:include page="/footer" />


<script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

<script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/assets/js/bootstrap-select.min.js"></script>
<script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

<script src="/resources/assets/js/easypiechart.min.js"></script>
<script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

<script src="/resources/assets/js/owl.carousel.min.js"></script>

<script src="/resources/assets/js/wow.js"></script>

<script src="/resources/assets/js/icheck.min.js"></script>
<script src="/resources/assets/js/price-range.js"></script>

<script src="/resources/assets/js/main.js"></script>

</body>
</html>