<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-Wóz | Dodawanie Samochodu </title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">

    <%-- jQuery UI CSS --%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
</head>
<body>

<jsp:include page="/menu" />

<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title text-center">Dodaj Samochod</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->


<!-- register-area -->
<div class="register-area" style="background-color: rgb(249, 249, 249);">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <div class="box-for overflow">
                <div class="col-md-12 col-xs-12 register-blocks">
                    <br>
                    <div class="form-group col-md-6">
                    <%--<form type="file" class="form-control" id="photos" formenctype="multipart/form-data" required="required">--%>
                    <form:form id="upload-file-form" action="/car/addCarPhotos" method="post"
                                modelAttribute="uploadForm" enctype="multipart/form-data">
                        <input id="addFile" type="button" value="Zdjęcie samochodu" />
                        <table id="fileTable">

                        </table>
                    </form:form>
                    </div>

                    <form action="" method="post">


                        <div class="form-group col-md-6">
                            <label for="brand">Marka</label>
                            <input type="text" class="form-control" id="brand" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="model">Model</label>
                            <input type="text" class="form-control" id="model" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="carMileage">Przebieg</label>
                            <input type="number" class="form-control" id="carMileage" required="required" min="0" max="999999">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fuel">Rodzaj Paliwa</label>
                            <input type="text" class="form-control" id="fuel" required="required">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="carPower">Moc</label>
                            <input type="number" class="form-control" id="carPower" required="required" min="1" max="2000">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="yearOfProduction">Rok Produkcji</label>
                            <input type="number" class="form-control" id="yearOfProduction" required="required" min="1950" max="2018">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="color">Kolor</label>
                            <select class="form-control" id="color">
                                <option>Wybierz</option>
                                <option>Biały</option>
                                <option>Szary</option>
                                <option>Czarny</option>
                                <option>Czerwony</option>
                                <option>Żółty</option>
                                <option>Zielony</option>
                                <option>Niebieski</option>
                                <option>Fioletowy</option>
                                <option>Pomarańczony</option>
                                <option>Brązowy</option>
                                <option>Różowy</option>
                                <option>Złoty</option>
                                <option>Srebrny</option>
                                <option>Seledynowy</option>
                                <option>Granatowy</option>
                                <option>Kasztanowy</option>
                                <option>Limonkowy</option>
                                <option>Morski</option>
                                <option>Oliwkowy</option>
                                <option>Purpurowy</option>
                                <option>Cyjanowy</option>
                                <option>Fuksjowy</option>
                                <option>Indygowy</option>
                                <option>Magentowy</option>
                                <option>Beżowy</option>
                                <option>Ecru</option>
                                <option>Akwamarynowy</option>
                                <option>Amarantowy</option>
                                <option>Cielisty</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="isAccidentFree">Bezwypadkowy</label>
                            <select class="form-control" id="isAccidentFree">
                                <option>Wybierz</option>
                                <option>Tak</option>
                                <option>Nie</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="isInsured">Ubezpieczony</label>
                            <select class="form-control" id="isInsured">
                                <option>Wybierz</option>
                                <option>Tak</option>
                                <option>Nie</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="drivingGear">Rodzaj Napędu</label>
                            <select class="form-control" id="drivingGear">
                                <option>Wybierz</option>
                                <option>Napęd tylny</option>
                                <option>Napęd przedni</option>
                                <option>Napęd na wszystkie koła</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="hasAirConditioniing">Klimatyzacja</label>
                            <select class="form-control" id="hasAirConditioniing">
                                <option>Wybierz</option>
                                <option>Tak</option>
                                <option>Nie</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="hasCentralLock">Centralny zamek</label>
                            <select class="form-control" id="hasCentralLock">
                                <option>Wybierz</option>
                                <option>Tak</option>
                                <option>Nie</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="hasParkingSensor">Czujnik parkowania</label>
                            <select class="form-control" id="hasParkingSensor">
                                <option>Wybierz</option>
                                <option>Tak</option>
                                <option>Nie</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="price">Cena</label>
                            <input type="number" class="form-control" id="price" required="required" min="0">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="description">Opis</label>
                            <textarea class="form-control" rows="10" id="description"></textarea>
                        </div>

                        <div class="text-center">
                            <button id="submitFormButton" type="submit" class="btn btn-default">Dodaj samochód</button>
                        </div>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>


<jsp:include page="/footer" />


<script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

<script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/assets/js/bootstrap-select.min.js"></script>
<script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

<script src="/resources/assets/js/easypiechart.min.js"></script>
<script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

<script src="/resources/assets/js/owl.carousel.min.js"></script>

<script src="/resources/assets/js/wow.js"></script>

<script src="/resources/assets/js/icheck.min.js"></script>
<script src="/resources/assets/js/price-range.js"></script>

<script src="/resources/assets/js/main.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        //add more file components if Add is clicked
        $('#addFile').click(function() {
            var fileIndex = $('#fileTable tr').children().length - 1;
            $('#fileTable').append(
                '<tr><td>'+
                '	<input type="file" name="files['+ fileIndex +']" />'+
                '</td></tr>');
        });

        $("#submitFormButton").click(function(event){
            event.preventDefault();
            var data = $("#upload-file-form");
            $.ajax({
                url: "http://" + window.location.host + "/car/addCarPhotos",
                type: "POST",
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(){
                    $("#submitFormButton").submit();
                },
                error: function(err, textErr){
                    console.log(err + " " + textErr);
                }

            });
        })
    });

//    $(document).on('change', '.btn-file :file', function() {
//        console.log("Script");
//        var input = $(this),
//            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//        input.trigger('fileselect', [label]);
//    });
//
//    $('.btn-file :file').on('fileselect', function(event, label) {
//
//        var input = $(this).parents('.input-group').find(':text'),
//            log = label;
//
//        if( input.length ) {
//            input.val(log);
//        } else {
//            if( log ) alert(log);
//        }
//
//    });

//    function readURL(input) {
//        if (input.files && input.files[0]) {
//            var reader = new FileReader();
//
//            reader.onload = function (e) {
//                $('#img-upload').attr('src', e.target.result);
//            }
//
//            reader.readAsDataURL(input.files[0]);
//        }
//
//        var div = document.createElement('div');
//
//        div.className = 'col-md-3';
//
//        div.innerHTML = ' <div class="form-group">\n' +
//            '                <div class="input-group">\n' +
//            '                    <span class="input-group-btn">\n' +
//            '                        <span class="btn btn-warning text-white btn-file">\n' +
//            '                            Wybierz... <input type="file" accept="image/png, image/jpeg, image/gif" id="imgInp2">\n' +
//            '                        </span>\n' +
//            '                    </span>\n' +
//            '                    <input id=\'urlname\'type="text" class="form-control" readonly>\n' +
//            '                </div>\n' +
//            '                <div id="auctionImages">\n' +
//            '                    <img id=\'img-upload\' name="imageSingleAuction"/>\n' +
//            '                </div>\n' +
//            '            </div>';
//
//        document.getElementById('imageRow').appendChild(div);
//    }

    <%--$(document).ready(function(){--%>
        <%--$("#subbutton").click(function(){--%>
            <%--processFileUpload();--%>
        <%--});--%>

        <%--$("#upload-file-input").on('change',prepareLoad);--%>
        <%--var files;--%>
        <%--function prepareLoad(event)--%>
        <%--{--%>
            <%--console.log(' event fired'+event.target.files[0].name);--%>
            <%--files=event.target.files;--%>
        <%--}--%>
        <%--function processFileUpload()--%>
        <%--{--%>
            <%--console.log("fileupload clicked");--%>
            <%--var oMyForm = new FormData();--%>
            <%--oMyForm.append("file", files[0]);--%>
            <%--$.ajax({dataType : 'json',--%>
                    <%--url : "${pageContext.request.contextPath}/car/uploadMyFile",--%>
                    <%--data : oMyForm,--%>
                    <%--type : "POST",--%>
                    <%--enctype: 'multipart/form-data',--%>
                    <%--processData: false,--%>
                    <%--contentType:false,--%>
                    <%--success : function(result) {--%>
                        <%--alert('success'+JSON.stringify(result));--%>
                    <%--},--%>
                    <%--error : function(result){--%>
                        <%--alert('error'+JSON.stringify(result));--%>
                    <%--}--%>
                <%--});--%>
        <%--}--%>
    <%--});--%>


//
//    $('#upload-file-form').change(function () {
//        console.log("Change");
//
//
//        $.ajax({
//            url: "http://" + window.location.host + "/car/uploadFile",
//            type: "POST",
//            data: new FormData($("#upload-file-form")[0]),
//            enctype: 'multipart/form-data',
//            processData: false,
//            cache: false,
//            success: function () {
//                console.log("Success");
//            },
//            error: function () {
//                console.log("Error");
//
//            }
//        });
//    });

</script>

</body>
</html>