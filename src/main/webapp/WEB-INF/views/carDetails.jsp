<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 14.12.2017
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-Wóz</title>
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/lightslider.min.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

<jsp:include page="/menu" />

<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Szczegóły pojazdu</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<!-- property area -->
<div class="content-area single-property" style="background-color: #FCFCFC;">&nbsp;
    <div class="container">

        <div class="clearfix padding-top-40" >

            <div class="col-md-12 single-property-content prp-style-1 ">
                <div class="row">
                    <div class="light-slide-item">
                        <div class="clearfix">
                            <div class="favorite-and-print">
                                <a class="add-to-fav" href="#login-modal" data-toggle="modal">
                                    <i class="fa fa-star-o"></i>
                                </a>
                                <a class="printer-icon " href="javascript:window.print()">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
                            <%--<ul id="image-gallery" class="gallery list-unstyled cS-hidden">--%>
                                <%--<c:forEach items="${params}" var="unit" varStatus="loopCounter">--%>
                                    <%--<li data-thumb="<c:url value="/resources/assets/img/cars/id-${unit.id}-zdj${loopCounter.count}" />">--%>
                                        <%--<img src="<c:url value="/resources/assets/img/cars/id-${unit.id}-zdj${loopCounter.count}" />" />--%>
                                    <%--</li>--%>
                            <%--</c:forEach>--%>
                            <%--</ul>--%>


                            <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                <li data-thumb="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj1.jpg" />">
                                    <img src="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj1.jpg" />" />
                                </li>
                                <li data-thumb="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj2.jpg" />">
                                    <img src="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj2.jpg" />" />
                                </li>
                                <li data-thumb="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj3.jpg" />">
                                    <img src="<c:url value="/resources/assets/img/cars/id-${params.carId}-zdj3.jpg" />" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="single-property-wrapper">
                    <div class="single-property-header">
                        <h1 class="property-title pull-left">${params.brand}</h1>
                        <span class="property-price pull-right">${params.price} zl</span>
                    </div>

                    <div class="col-xs-4 col-sm-3 col-md-2 p-b-15 pull-right">
                        <button onclick="addToBasket();" class="btn btn-default">Do koszyka</button>
                    </div>

                    <div class="property-meta entry-meta clearfix ">

                        <div class="col-xs-6 col-sm-3 col-md-3 p-b-15">
                                    <span class="property-info-icon icon-tag">
                                        <img class="icon" src="/resources/assets/img/icon/car/icons8-road-filled-y-50.png">
                                    </span>
                            <span class="property-info-entry">
                                        <span class="property-info-label">Przebieg</span>
                                        <span class="property-info-value">${params.mileage} km</span>
                                    </span>
                        </div>

                        <div class="col-xs-6 col-sm-3 col-md-3 p-b-15">
                                    <span class="property-info icon-area">
                                        <img src="/resources/assets/img/icon/car/icons8-gas-station-filled-y-50.png">
                                    </span>
                            <span class="property-info-entry">
                                        <span class="property-info-label">Paliwo</span>
                                        <span class="property-info-value">${params.fuel}</span>
                                    </span>
                        </div>

                        <div class="col-xs-6 col-sm-3 col-md-3 p-b-15">
                                    <span class="property-info-icon icon-bed">
                                        <img src="/resources/assets/img/icon/car/icons8-engine-filled-y-50.png">
                                    </span>
                            <span class="property-info-entry">
                                        <span class="property-info-label">Moc</span>
                                        <span class="property-info-value">${params.power} KM</span>
                                    </span>
                        </div>

                        <div class="col-xs-6 col-sm-3 col-md-3 p-b-15">
                                    <span class="property-info-icon icon-bed">
                                        <img src="/resources/assets/img/icon/car/icons8-factory-filled-y-50.png">
                                    </span>
                            <span class="property-info-entry">
                                        <span class="property-info-label">Rok produkcji</span>
                                        <span class="property-info-value">${params.yearOfProduction}</span>
                                    </span>
                        </div>
                    </div>
                    <!-- .property-meta -->

                    <div class="section">
                        <h4 class="s-property-title">Opis</h4>
                        <div class="s-property-content">
                            <p>${params.description}</p>
                        </div>
                    </div>
                    <!-- End description area  -->

                    <div class="section additional-details">

                        <h4 class="s-property-title">Dodatkowe szczegóły</h4>

                        <ul class="additional-details-list clearfix">
                            <li>
                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Kolor</span>
                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">${params.color}</span>
                            </li>

                            <li>
                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Bezwypadkowy</span>
                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">
                                    <c:choose>
                                        <c:when test="${params.isAccidentFree()}">Tak</c:when>
                                        <c:otherwise>Nie</c:otherwise>
                                    </c:choose>
                                </span>
                            </li>
                            <li>
                                <span class="col-xs-6 col-sm-4 col-md-4 add-d-title">Ubezpieczony</span>
                                <span class="col-xs-6 col-sm-8 col-md-8 add-d-entry">
                                    <c:choose>
                                        <c:when test="${params.isInsured()}">Tak</c:when>
                                        <c:otherwise>Nie</c:otherwise>
                                    </c:choose>
                                </span>
                            </li>

                        </ul>
                    </div>
                    <!-- End additional-details area  -->

                    <div class="section property-features">

                        <h4 class="s-property-title">Dodatki</h4>
                        <ul>
                            <li><a href="properties.html">${params.drivingGear}</a></li>
                            <c:if test="${params.isHasAirConditioning()}">
                                <li><a href="properties.html">Klimatyzacja</a></li>
                            </c:if>
                            <c:if test="${params.isHasCentralLock()}">
                                <li><a href="properties.html">Centralny zamek</a></li>
                            </c:if>
                            <c:if test="${params.isHasParkingSensor()}">
                                <li><a href="properties.html">Czujnik parkowania</a></li>
                            </c:if>
                        </ul>

                    </div>
                    <!-- End features area  -->


                    <div class="section property-share">
                        <h4 class="s-property-title">Podziel się ze znajomymi </h4>
                        <div class="roperty-social">
                            <ul>
                                <li><a title="Share this on dribbble " href="#"><img src="/resources/assets/img/social_big/dribbble_grey.png"></a></li>
                                <li><a title="Share this on facebok " href="#"><img src="/resources/assets/img/social_big/facebook_grey.png"></a></li>
                                <li><a title="Share this on delicious " href="#"><img src="/resources/assets/img/social_big/delicious_grey.png"></a></li>
                                <li><a title="Share this on tumblr " href="#"><img src="/resources/assets/img/social_big/tumblr_grey.png"></a></li>
                                <li><a title="Share this on digg " href="#"><img src="/resources/assets/img/social_big/digg_grey.png"></a></li>
                                <li><a title="Share this on twitter " href="#"><img src="/resources/assets/img/social_big/twitter_grey.png"></a></li>
                                <li><a title="Share this on linkedin " href="#"><img src="/resources/assets/img/social_big/linkedin_grey.png"></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<jsp:include page="/footer" />

<script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>
<script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/assets/js/bootstrap-select.min.js"></script>
<script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>
<script src="/resources/assets/js/easypiechart.min.js"></script>
<script src="/resources/assets/js/jquery.easypiechart.min.js"></script>
<script src="/resources/assets/js/owl.carousel.min.js"></script>
<script src="/resources/assets/js/wow.js"></script>
<script src="/resources/assets/js/icheck.min.js"></script>
<script src="/resources/assets/js/price-range.js"></script>
<script type="text/javascript" src="/resources/assets/js/lightslider.min.js"></script>
<script src="/resources/assets/js/main.js"></script>

<script>
    $(document).ready(function () {

        $('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 9,
            slideMargin: 0,
            speed: 500,
            auto: false,
            loop: true,
            onSliderLoad: function () {
                $('#image-gallery').removeClass('cS-hidden');
            }
        });
    });

    function addToBasket(){
        $.ajax({
            url: "http://" + window.location.host + "/basket/add?id=" + ${params.carId},
            type: "GET",
            success: function(){
                window.location.href = "/home";
            },
            error: function(err, text){
                console.log(err + " " + text);
            }
        })
    }
</script>

</body>
</html>
