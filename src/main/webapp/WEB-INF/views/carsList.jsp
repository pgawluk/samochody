<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-wóz</title>
    <meta name="description" content="Złotówa - twoje miejsce z samochodami">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- Body content -->

    <jsp:include page="/menu" />

    <div class="page-head">
        <div class="container">
            <div class="row">
                <div class="page-head-content">
                    <c:choose>
                        <c:when test="${favouritesList}">
                            <h1 class="page-title text-center">Ulubione samochody</h1>
                        </c:when>
                        <c:otherwise>
                            <h1 class="page-title text-center">Wyszukiwarka pojazdów</h1>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>

    <!-- property area -->
    <div class="properties-area recent-property" style="background-color: #FFF;">
        <div class="container">
            <div class="row  pr0 padding-top-40 properties-page">
                <c:if test="${!favouritesList}" >
                    <div class="col-md-12 padding-bottom-40 large-search">
                        <div class="search-form wow pulse">
                            <form action="" class=" form-inline">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Słowo kluczowe">
                                    </div>
                                    <div class="col-md-2">
                                        <select id="car-make" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Wybierz markę">
                                            <option>Opel</option>
                                            <option>Volkswagen</option>
                                            <option>Audi</option>
                                            <option>Toyota</option>
                                            <option>Ford</option>
                                            <option>Lexus</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="gearbox" class="selectpicker show-tick form-control" title="Rodzaj skrzyni">
                                            <option>Ręczna</option>
                                            <option>Automatyczna</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="state" class="selectpicker show-tick form-control" title="Wybierz stan">
                                            <option>Nowy</option>
                                            <option>Używany</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select id="gas-type" class="selectpicker show-tick form-control" title="Wybierz paliwo">
                                            <option>Benzyna</option>
                                            <option>Diesel</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="search-row">

                                        <div class="col-sm-3">
                                            <label for="price-range">Zakres cenowy (PLN):</label>
                                            <input type="text" class="span2" value="" data-slider-min="2000"
                                                   data-slider-max="50000" data-slider-step="500"
                                                   data-slider-value="[7000,14000]" id="price-range" ><br />
                                            <b class="pull-left color">2000 PLN</b>
                                            <b class="pull-right color">50000 PLN</b>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <label for="production-date">Rok produkcji:</label>
                                            <input type="text" class="span2" value="" data-slider-min="1990"
                                                   data-slider-max="2017" data-slider-step="1"
                                                   data-slider-value="[1995,2000]" id="production-date" ><br />
                                            <b class="pull-left color">1990</b>
                                            <b class="pull-right color">2017</b>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <label for="mileage">Przebieg :</label>
                                            <input type="text" class="span2" value="" data-slider-min="0"
                                                   data-slider-max="500000" data-slider-step="25000"
                                                   data-slider-value="[0,100000]" id="mileage" ><br />
                                            <b class="pull-left color">0 km</b>
                                            <b class="pull-right color">500 000 km</b>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <label for="power">Moc silnika :</label>
                                            <input type="text" class="span2" value="" data-slider-min="50"
                                                   data-slider-max="200" data-slider-step="10"
                                                   data-slider-value="[70,100]" id="power" ><br />
                                            <b class="pull-left color">50 KM</b>
                                            <b class="pull-right color">200 KM</b>
                                        </div>
                                        <!-- End of  -->

                                    </div>

                                    <div class="search-row">

                                        <h3>Wygoda</h3>
                                        <hr>

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> 4x4
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Czujnik parkowania
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Centralny zamek
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Elektryczne szyby
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Podgrzewane fotele
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Klimatyzacja
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Wspomaganie kierownicy
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Nawigacja GPS
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->
                                    </div>
                                    <div class="search-row">
                                        <h3>Bezpieczeństwo</h3>
                                        <hr>

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> ABS
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> ASR
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Autoalarm
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Immobiliser
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Kontrola trakcji
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->

                                        <div class="col-sm-3">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Poduszki powietrzne
                                                </label>
                                            </div>
                                        </div>
                                        <!-- End of  -->
                                    </div>
                                </div>
                                <div class="center">
                                    <button type="submit" value="" class="btn btn-default">
                                        <i class="glyphicon glyphicon-search"></i> Szukaj
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </c:if>

                <div class="col-md-12  clear">
                    <div class="col-xs-10 page-subheader sorting pl0">
                        <ul class="sort-by-list">
                            <li class="active">
                                <a href="javascript:void(0);" class="order_by_date" data-orderby="property_date" data-order="ASC">
                                    Sortuj wg daty dodania <i class="fa fa-sort-amount-asc"></i>
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:void(0);" class="order_by_price" data-orderby="property_price" data-order="DESC">
                                    Sortuj wg ceny <i class="fa fa-sort-numeric-desc"></i>
                                </a>
                            </li>
                        </ul><!--/ .sort-by-list-->

                        <div class="items-per-page">
                            <label for="items_per_page"><b>Ofert na stronie :</b></label>
                            <div class="sel">
                                <select id="items_per_page" name="per_page">
                                    <option value="3">3</option>
                                    <option value="6">6</option>
                                    <option value="9">9</option>
                                    <option selected="selected" value="12">12</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="60">60</option>
                                </select>
                            </div><!--/ .sel-->
                        </div><!--/ .items-per-page-->
                    </div>

                    <div class="col-xs-2 layout-switcher">
                        <a class="layout-list" href="javascript:void(0);"> <i class="fa fa-th-list"></i>  </a>
                        <a class="layout-grid active" href="javascript:void(0);"> <i class="fa fa-th"></i> </a>
                    </div><!--/ .layout-switcher-->
                </div>

                <form:form modelAttribute="availableCarsList">
                    <div class="col-md-12 clear ">
                        <div id="list-type" class="proerty-th">
                            <c:forEach items="${availableCarsList}" var="car" varStatus="status">
                                    <div class="col-sm-6 col-md-3 p0">
                                        <div class="box-two proerty-item">
                                            <div class="item-thumb">
                                                <a href="/carDetails/${car.carId}" ><img style="object-fit: cover" src="/resources/assets/img/auta/polo.jpg"></a>
                                            </div>
                                            <div class="item-entry overflow">
                                                <h5>
                                                    <a href="/carDetails/${car.carId}" > ${car.brand} ${car.model} </a>
                                                    <span class="pull-right" style="margin-left: 5px;">
                                                        <a href="/car/addToFavourites?carId=${car.carId}" >
                                                            <img height="20px" width="20px" src="/resources/assets/img/icon/icons8-love-filled-25.png" >
                                                        </a>
                                                    </span>
                                                    <span class="pull-right" style="margin-left: 5px;">
                                                        <span onclick="addToBasket(${car.carId})">
                                                            <img height="20px" width="20px" src="/resources/assets/img/icon/icons8-shopping-cart-filled-25.png" >
                                                        </span>
                                                    </span>
                                                </h5>
                                                <div class="dot-hr"></div>
                                                <span class="pull-left"><b>Rocznik :</b> ${car.yearOfProduction} </span>
                                                <span class="proerty-price pull-right">${car.price} PLN</span>
                                            </div>
                                        </div>
                                    </div>
                            </c:forEach>
                        </div>
                    </div>
                </form:form>

                <%--<div class="col-md-12 clear">--%>
                    <%--<div class="pull-right">--%>
                        <%--<div class="pagination">--%>
                            <%--<ul>--%>
                                <%--<li><a href="#">Poprzednia</a></li>--%>
                                <%--<li><a href="#">1</a></li>--%>
                                <%--<li><a href="#">2</a></li>--%>
                                <%--<li><a href="#">3</a></li>--%>
                                <%--<li><a href="#">4</a></li>--%>
                                <%--<li><a href="#">Następna</a></li>--%>
                            <%--</ul>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
            </div>
        </div>
    </div>

    <jsp:include page="/footer" />


    <script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

    <script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/assets/js/bootstrap-select.min.js"></script>
    <script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

    <script src="/resources/assets/js/easypiechart.min.js"></script>
    <script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

    <script src="/resources/assets/js/owl.carousel.min.js"></script>

    <script src="/resources/assets/js/wow.js"></script>

    <script src="/resources/assets/js/icheck.min.js"></script>
    <script src="/resources/assets/js/price-range.js"></script>

    <script src="/resources/assets/js/main.js"></script>

    <script type="text/javascript">
        function addToBasket(carId){
            $.ajax({
                url: "http://" + window.location.host + "/basket/add?id=" + carId,
                type: "GET",
                success: function(){
                    window.location.href = "/home";
                },
                error: function(err, text){
                    console.log(err + " " + text);
                }
            })
        }
    </script>
</body>
</html>