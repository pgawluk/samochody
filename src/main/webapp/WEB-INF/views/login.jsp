<%--
  Created by IntelliJ IDEA.
  User: zloty
  Date: 2017-12-01
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GARO ESTATE | Register page</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">

    <%-- jQuery UI CSS --%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
</head>
<body>

    <jsp:include page="/menu" />

    <div class="page-head">
        <div class="container">
            <div class="row">
                <div class="page-head-content">
                    <h1 class="page-title text-center">Logowanie</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End page header -->


    <div class="register-area" style="background-color: rgb(249, 249, 249);">
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div class="box-for overflow">
                    <div class="col-md-12 col-xs-12 login-blocks">
                        <br>
                        <form role="form" name="loginForm" action="/login" method="post">
                            <div class="form-group">
                                <label for="email">Adres e-mail</label>
                                <input type="email" class="form-control" id="email" name="email" autofocus required>
                            </div>
                            <div class="form-group">
                                <label for="password">Hasło</label>
                                <input type="password" class="form-control" id="password" name='password' required>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default">Zaloguj</button>
                            </div>
                        </form>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <jsp:include page="/footer" />


    <script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

    <script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/assets/js/bootstrap-select.min.js"></script>
    <script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

    <script src="/resources/assets/js/easypiechart.min.js"></script>
    <script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

    <script src="/resources/assets/js/owl.carousel.min.js"></script>

    <script src="/resources/assets/js/wow.js"></script>

    <script src="/resources/assets/js/icheck.min.js"></script>
    <script src="/resources/assets/js/price-range.js"></script>

    <script src="/resources/assets/js/main.js"></script>


    <%--
            Modal z alertem - blad z bazy (np. konflikt bo zly login i haslo)
            Musi byc zaimportowany JQuery UI (pod spodem skrypt, a na gorze jest jeszcze CSS
         --%>

    <%-- jQuery UI javascript --%>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <c:if test="${not empty wrongCredentials}">
        <jsp:include page="/modal/alert/wrongCredentials" />
        <script type="text/javascript">
            <c:if test="${not empty wrongCredentials}">
                $("[name=alertDialog]").dialog({
                    autoOpen: false,
                    modal: true
                });
                $("[name=alertClose]").on("click", function() {
                    $("[name=alertDialog]").dialog("close");
                });
                $("[name=alertDialog]").dialog("open");
            </c:if>

            // jesli zwrotka ze email nieunikalny to:
            $('[name=email]').val('');
            $('[name=password]').val('');
        </script>
    </c:if>
</body>
</html>
