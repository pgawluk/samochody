<%@taglib prefix="sec"
          uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav class="navbar navbar-default ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"><img src="/resources/assets/img/logo.png" alt=""></a>
        </div>

        <div class="collapse navbar-collapse yamm" id="navigation">
            <div class="button navbar-right">

                <%-- niezalogowany --%>
                <sec:authorize access="!hasRole('ROLE_USER') and !hasRole('ROLE_ADMIN') and !hasRole('ROLE_MANAGER')">
                    <button class="navbar-btn nav-button wow bounceInRight login" onclick=" window.open('/user/login', '_self')" data-wow-delay="0.45s">Zaloguj</button>
                    <button class="navbar-btn nav-button wow fadeInRight" onclick=" window.open('/user/register', '_self')" data-wow-delay="0.48s">Zarejestruj Konto</button>
                </sec:authorize>

                <%-- zalogowany --%>
                <sec:authorize access="hasRole('ROLE_USER') or hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')">
                    <form role="form" action="/logout" method="post" id="logout-form">
                        <button class="navbar-btn nav-button wow bounceInRight login" type="button" onclick=" window.open('/user/account', '_self')" data-wow-delay="0.45s">Zarządzaj Kontem</button>
                        <button class="navbar-btn nav-button wow fadeInRight" type="submit" data-wow-delay="0.48s">Wyloguj</button>
                    </form>
                </sec:authorize>

            </div>
            <ul class="main-nav nav navbar-nav navbar-right">
                <li class="wow fadeInDown" data-wow-delay="0.1s">
                    <a href="/" >Strona Główna</a>
                </li>

                <%-- niezalogowany --%>
                <sec:authorize access="!hasRole('ROLE_USER') and !hasRole('ROLE_ADMIN') and !hasRole('ROLE_MANAGER')">
                    <li class="wow fadeInDown" data-wow-delay="0.3s"><a href="/car/carsList">Nasza Oferta</a></li>
                    <li class="wow fadeInDown" data-wow-delay="0.3s"><a href="/contact">Kontakt</a></li>
                </sec:authorize>

                <%-- zalogowany jako uzytkownik --%>
                <sec:authorize access="hasRole('ROLE_USER')">
                    <li class="dropdown ymm-sw " data-wow-delay="0.2s">
                    <%--<li class="wow fadeInDown dropdown ymm-sw " data-wow-delay="0.2s">--%>
                        <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Nasza Oferta<b class="caret"></b></a>
                        <ul class="dropdown-menu navbar-nav">
                            <li>
                                <a href="/car/carsList">Przeglądaj Samochody</a>
                            </li>
                            <li>
                                <a href="/car/orderCar">Zamów Samochód</a>
                            </li>
                            <li>
                                <a href="/car/carsList/favouriteCars">Ulubione samochody</a>
                            </li>
                        </ul>
                    </li>
                    <li class="wow fadeInDown" data-wow-delay="0.3s"><a href="/contact">Kontakt</a></li>
                    <li class="wow fadeInDown" data-wow-delay="0.4s">
                        <a class="" href="/basket">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Twój Koszyk
                        </a>
                    </li>
                </sec:authorize>

                <%-- zalogowany jako kierownik --%>
                <sec:authorize access="hasRole('ROLE_MANAGER')">
                    <li class="dropdown ymm-sw " data-wow-delay="0.2s">
                    <%--<li class="wow fadeInDown dropdown ymm-sw " data-wow-delay="0.2s">--%>
                        <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Samochody<b class="caret"></b></a>
                        <ul class="dropdown-menu navbar-nav">
                            <li>
                                <a href="/car/addCar">Dodaj nowy</a>
                            </li>
                            <li>
                                <a href="/car/carsList">Samochody Dostępne</a>
                            </li>
                            <li>
                                <a href="/managers/boughtCars">Samochody Sprzedane</a>
                            </li>
                            <li>
                                <a href="/car/carsList">Samochody Zamówione</a>
                            </li>
                        </ul>
                    </li>
                    <li class="wow fadeInDown" data-wow-delay="0.3s"><a href="/customersList">Lista Klientów</a></li>
                    <li class="wow fadeInDown" data-wow-delay="0.4s">
                        <a class="" href="/reports/reportsList">
                            <span class="glyphicon glyphicon-list-alt"></span> Lista Raportów
                        </a>
                    </li>
                </sec:authorize>

                <%-- zalogowany jako admin --%>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <li class="dropdown ymm-sw " data-wow-delay="0.2s">
                    <%--<li class="wow fadeInDown dropdown ymm-sw " data-wow-delay="0.2s">--%>
                        <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Pracownicy<b class="caret"></b></a>
                        <ul class="dropdown-menu navbar-nav">
                            <li>
                                <a href="/employee/addEmployee">Dodaj Pracownika</a>
                            </li>
                            <li>
                                <a href="/employee/employeesList">Lista Pracowników</a>
                            </li>
                        </ul>
                    </li>
                    <li class="wow fadeInDown" data-wow-delay="0.3s"><a href="/customersList">Lista Klientów</a></li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</nav>


<%-- skrypt do wylogowania --%>
<script type="text/javascript">
    function logoutSubmit() {
        $('#logoutForm').submit();
    }
</script>