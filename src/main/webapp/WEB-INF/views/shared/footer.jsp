<!-- Footer area-->
<div class="footer-area">

    <div class=" footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>O nas</h4>
                        <div class="footer-title-line"></div>
                        <img src="/resources/assets/img/footer-logo.png" alt="" class="wow pulse" data-wow-delay="1s">
                        <ul class="footer-adress">
                            <li><i class="pe-7s-map-marker strong"> </i> Adres</li>
                            <li><i class="pe-7s-mail strong"> </i> mail@mail.pl</li>
                            <li><i class="pe-7s-call strong"> </i> +48 100 100 100</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-md-offset-6 wow fadeInRight animated">
                    <div class="single-footer">
                        <h4>Szybkie linki </h4>
                        <div class="footer-title-line"></div>
                        <ul class="footer-menu">
                            <li><a href="contact.html">Skontaktuj się z nami</a></li>
                            <li><a href="faq.html">Regulamin</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-copy text-center">
        <div class="container">
            <div class="row">
                <div class="pull-left">
                    <span> (C) <a href="http://www.KimaroTec.com">Super-wóz</a> , Wszelkie prawa zastrzeżone 2017  </span>
                </div>
                <div class="bottom-menu pull-right">
                    <ul>
                        <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.2s">Strona główna</a></li>
                        <li><a class="wow fadeInUp animated" href="#" data-wow-delay="0.6s">Kontakt</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>