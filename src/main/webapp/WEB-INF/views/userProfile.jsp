<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-Wóz | Rejestracja</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">

    <%-- jQuery UI CSS --%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
</head>
<body>

    <jsp:include page="/menu" />

    <div class="page-head">
        <div class="container">
            <div class="row">
                <div class="page-head-content">
                    <h1 class="page-title text-center">Profil</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End page header -->

    <!-- property area -->
    <div class="content-area user-profiel" style="background-color: #FCFCFC;">&nbsp;
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 profiel-container">

                    <form:form modelAttribute="userProfile" method="get">
                        <div class="clear">
                            <div class="col-sm-3">
                            </div>

                            <div class="col-sm-3 padding-top-25">

                                <div class="form-group">
                                    <label>Imię: </label>
                                    <form:label path="firstname" >${userProfile.firstname}</form:label>
                                </div>

                            </div>
                            <div class="col-sm-3 padding-top-25">

                                <div class="form-group">
                                    <label>Nazwisko:</label>
                                    <form:label path="firstname" >${userProfile.lastname}</form:label>
                                </div>

                            </div>

                        </div>
                    </form:form>

                    <%--TODO: Dokonczyc obsluge w JS --%>
                    <form:form modelAttribute="userProfile" method="post" action="/user/account/edit" id="editPasswordAndEmail">
                        <div class="clear">
                            <br>
                            <hr>
                            <br>
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="form-group">
                                    <label>Hasło :</label>
                                    <input id="password" name="password" type="password" class="form-control" placeholder="haslo" required disabled="disabled" />
                                </div>
                                <div class="form-group">
                                    <label>Email :</label>
                                    <form:input path="email" name="email" type="email" class="form-control" placeholder="Email" required="required" disabled="true" />
                                </div>

                                <div class = "form-group" style="padding-top: 30px">
                                    <input type='button' class='btn btn-finish btn-primary btn-block' name='edit' value='Edytuj' />
                                </div>

                            </div>

                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label>Powtórz hasło :</label>
                                    <input name="confirmPassword" type="password" class="form-control" placeholder="Powtórz hasło" required disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Powtórz Email :</label>
                                    <input name="confirmEmail" type="email" class="form-control" placeholder="Powtórz Email" required disabled="disabled">
                                </div>
                                <div class = "form-group" style="padding-top: 30px">
                                    <input type='submit' form="editPasswordAndEmail" class='btn btn-finish btn-primary btn-block' name="save" value='Zapisz' />
                                </div>
                            </div>
                        </div>

                        <br>
                    </form:form>

                </div>
            </div><!-- end row -->

        </div>
    </div>



    <jsp:include page="/footer" />


    <script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

    <script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/assets/js/bootstrap-select.min.js"></script>
    <script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

    <script src="/resources/assets/js/easypiechart.min.js"></script>
    <script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

    <script src="/resources/assets/js/owl.carousel.min.js"></script>

    <script src="/resources/assets/js/wow.js"></script>

    <script src="/resources/assets/js/icheck.min.js"></script>
    <script src="/resources/assets/js/price-range.js"></script>

    <script src="/resources/assets/js/main.js"></script>

    <script type="text/javascript">
        $('input[name=edit]').click(function () {
           $('#editPasswordAndEmail :input').attr("disabled", false);
        });

        // sprawdz zgodnosc hasel
        $('[name=password], [name=confirmPassword]').on('keyup', function () {
            if(($('[name=password]').val() != "" && $('[name=confirmPassword]').val() != "") && ($('[name=password]').val() == $('[name=confirmPassword]').val())) {
                $('[name=password], [name=confirmPassword]').css('border-color', 'green');
            } else {
                $('[name=password], [name=confirmPassword]').css('border-color', 'red');
            }
        });
        // sprawdz zgodnosc maili
        $('[name=email], [name=confirmEmail]').on('keyup', function () {
            if(($('[name=email]').val() != "" && $('[name=confirmEmail]').val() != "") && ($('[name=email]').val() == $('[name=confirmEmail]').val())) {
                $('[name=email], [name=confirmEmail]').css('border-color', 'green');
            } else {
                $('[name=email], [name=confirmEmail]').css('border-color', 'red');
            }
        });

        $('[name=save]').on('click', function (e) {
            var result = true;

            if($('[name=password]').val() == '' || $('[name=confirmPassword]').val() == '' || $('[name=email]').val() == '' || $('[name=confirmEmail]').val() == ''){
                // zeby najpierw wyswietlalo podpowiedz "uzupelnij to pole"
            } else {
                if ($('[name=password]').val() != $('[name=confirmPassword]').val()) {
                    result = false;
                }
                if ($('[name=email]').val() != $('[name=confirmEmail]').val()) {
                    result = false;
                }
            }

            if(!result){
                alert("Uzupełnij poprawnie wszystkie pola");
                e.preventDefault();
            }
        })

    </script>



    <%--
        Modal z alertem - blad z bazy (np. konflikt bo email juz istnieje)
        Musi byc zaimportowany JQuery UI (pod spodem skrypt, a na gorze jest jeszcze CSS
     --%>

    <%-- jQuery UI javascript --%>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <c:if test="${dbError}">
        <jsp:include page="/modal/alert/dbError" />
        <script type="text/javascript">
            console.log("dberror = ${dbError}");
            <c:if test="${dbError}">
            $("[name=alertDialog]").dialog({
                autoOpen: false,
                modal: true
            });
            $("[name=alertClose]").on("click", function() {
                $("[name=alertDialog]").dialog("close");
            });
            $("[name=alertDialog]").dialog("open");
            </c:if>

            // jesli zwrotka ze email nieunikalny to:
            $("input").prop('disabled', false);
            $('[name=email]').val('');
        </script>
    </c:if>

</body>
</html>