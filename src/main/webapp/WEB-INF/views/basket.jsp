<%--
  Created by IntelliJ IDEA.
  User: przem
  Date: 13.12.2017
  Time: 16:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-wóz | Koszyk</title>
    <meta name="description" content="Złotówa - twoje miejsce z samochodami">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

<jsp:include page="/menu" />

<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Twój koszyk </h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<div>
    <div class="container">
        <div id="carList" class="wow fadeInRight row pr0 padding-top-40 padding-bottom-40" data-wow-delay="0.2s">

        </div>

        <!-- List summary -->
        <div class="row pr0 padding-top-40 padding-bottom-40">
            <row>
                <div class="col-xs-12" style="float: right">
                    <div class="col-xs-3" style="float: right">
                        <b>SUMA: </b><b id="price"></b>
                    </div>
                </div>
            </row>

            <row>
                <div class="col-xs-12" style="float: right">
                    <div class="col-xs-3">
                        <button onclick="window.location.href='/car/carsList'" style="width: 100%">Wracam do przeglądania</button>
                    </div>
                    <div class="col-xs-6"></div>
                    <div class="col-xs-3" style="float: right">
                        <button id="finalizeButton" onclick="window.location.href='/payment/'" style="background-color: #ffc500; width: 100%"><b>Płacę</b></button>
                    </div>
                </div>
            </row>

        </div>
    </div>
</div>

<jsp:include page="/footer" />

<script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>

<script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/assets/js/bootstrap-select.min.js"></script>
<script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>

<script src="/resources/assets/js/easypiechart.min.js"></script>
<script src="/resources/assets/js/jquery.easypiechart.min.js"></script>

<script src="/resources/assets/js/owl.carousel.min.js"></script>

<script src="/resources/assets/js/wow.js"></script>

<script src="/resources/assets/js/icheck.min.js"></script>
<script src="/resources/assets/js/price-range.js"></script>

<script src="/resources/assets/js/main.js"></script>

<script>
    $.ajax({
        url: "http://" + window.location.host + "/basket/fetch",
        type: "GET",
        success: function(data, textStatus){
            console.log(textStatus);

            function onMinusClick(event){
                price = price - event.data.carPrice;
                event.data.row.remove();
                $("#price").text(price);
                removeFromBasket(event.data.carId);
            }

            var cars = JSON.parse(data);
            if(cars != null && cars.length !== 0) {
                $("#finalizeButton").attr("disabled", false);
                var price = 0;
                for (var i = 0; i < cars.length; i++) {
                    var car = cars[i];
                    var row = $("<row id=" + car.carId + "></row>");
                    var wrapper = $("<div class='col-xs-12'></div>");
                    row.append(wrapper);
                    var carInfo = car.brand + ' ' + car.model + ' ' + car.yearOfProduction;
                    var name = $("<div class='col-xs-11'>" + carInfo + "</div>");
                    wrapper.append(name);
                    var minus = $("<div class='col-xs-1'><i class='glyphicon glyphicon-minus'></i></div>");
                    wrapper.append(minus);
                    wrapper.append("<div class=\"col-xs-12\"><hr></div>");
                    minus.on("click", {carId: car.carId, carPrice: car.price, row: row}, onMinusClick);
                    price = price + car.price;
                    $("#carList").append(row);
                }
                $("#price").text(price);
            }
            else{
                $("#finalizeButton").attr("disabled", true);
            }
        },
        error: function(jqXHR, textStatus, errorThrown ){
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });

    function removeFromBasket(carId){
        $.ajax({
            url: "http://" + window.location.host + "/basket/remove?id=" + carId,
            type: "GET",
            success: function(){
                console.log("removed");
            },
            error: function (error, textError) {
                console.log(error + " " + textError);
            }
        })
    }


</script>

</body>
</html>
</html>
