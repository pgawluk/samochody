<%--
  Created by IntelliJ IDEA.
  User: przem
  Date: 14.12.2017
  Time: 22:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Super-Wóz | Strona płatności</title>
    <meta name="description" content="GARO is a real-estate template">
    <meta name="author" content="Kimarotec">
    <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico  the root directory -->
    <link rel="shortcut icon" href="/resources/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/resources/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/resources/assets/css/normalize.css">
    <link rel="stylesheet" href="/resources/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/resources/assets/css/fontello.css">
    <link href="/resources/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
    <link href="/resources/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
    <link href="/resources/assets/css/animate.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/resources/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/assets/css/icheck.min_all.css">
    <link rel="stylesheet" href="/resources/assets/css/price-range.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.theme.css">
    <link rel="stylesheet" href="/resources/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/resources/assets/css/style.css">
    <link rel="stylesheet" href="/resources/assets/css/responsive.css">
</head>
<body>

<jsp:include page="/menu" />

<div class="page-head">
    <div class="container">
        <div class="row">
            <div class="page-head-content">
                <h1 class="page-title">Finalizacja transakcji</h1>
            </div>
        </div>
    </div>
</div>
<!-- End page header -->

<!-- property area -->
<div class="content-area submit-property" style="background-color: #FCFCFC;">&nbsp;
    <div class="container">
        <div class="clearfix" >
            <div class="wizard-container">

                <div class="wizard-card ct-wizard-orange" id="wizardProperty">
                    <form action="" method="">
                        <div class="wizard-header">
                            <h3>
                                <b>Zapłać</b> za samochód <br>
                                <small>a już niedługo będziesz mógł się cieszyć nowym wózkiem!</small>
                            </h3>
                        </div>

                        <ul>
                            <li><a href="#step1" data-toggle="tab">Krok 1 </a></li>
                            <li><a href="#step2" data-toggle="tab">Krok 2 </a></li>
                            <li><a href="#step3" data-toggle="tab">Krok 3 </a></li>
                            <li><a href="#step4" data-toggle="tab">Podsumowanie </a></li>
                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane" id="step1">
                                <div class="row p-b-15  ">
                                    <h4 class="info-text"> Wybierz sposób dostawy lub odbioru</h4>
                                    <div  class="col-md-6 col-md-offset-3">
                                        <ul style=" padding: 0; margin: 0;">
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAXJSURBVHhe7ZtHqPVEGIavvTcQFAsiNkRsCFbsoq4UXCjIj0sRBHEjYlsoFhRBERfWhSLYXVjAhQUr4kJBsOHChr1XUGzvk/9+OJn/S07mTJKTA/PAA2cmU747NyeZTOasFAqFQqFQKBQKhfn4d4mdBF5gy+IkmFQwHSkDmEkZwEzKAGZSBjCTMoCZlAHMpAxgJmUAMykDmEkZwEzKADrsJk9e+3EmFnOf/ipfl5fIbWVnrIFFsqf8SP4uTyRjBuEfPoRfyeNkJ6zSothXfiYtju/klrKN3Ji9+jvI0+WrkmN/yGPlTHKDyeFAyX/bYvhNniRnkRtzW/0N5F2S4x/LzWQrucHMyyGSs836/1keLbuQG/Os+hvKdyRlziOjjdxg5uEo+aO0vn+Qh8mueDFvLG+Q4eWAz9dJjoV49WPOkpR5o0q10KWxkPVWnZfj5S/S+v1GHixT8GJmoCw/9loZYvltMOjfSsq1xtelMWN9eae8Vc4ziKdIrnPW5xdyP5mKF/PnkrzDq9RajpTkcSaGePU9bpaU4+9tpGtjXFzvkVb+JpnCaZI7m9X/RO4j58HaCPHyIKVszP6Sct/LTcnw6NoYA3iftPJ4vezCmfJPafU+lLvLebF2Qrw8SCnrweSasmdXKYeUxhjEh6TVwStlG+fIv6SVf1/uKnOwtkK8PEgp68FdmLLPVCmHlMZgI/mYtHp4hfQ4V/4trdzbckeZi7UX4uVBSlmPbSTXbf4O91uT0pjBHepJaXXxIhlygfxH2nGmA9vLPrA2Q7w8SCnbxL2S8ldVqYjUxoxN5NPS6uOFEi6WYf5rcjvZF9ZuiJcHKWWb4JGO8tz4mInUSG0sZHP5rLQ2OOPiM/MlubXsE2s7xMuDlLJNMGX7QFLnVDJCUhuLYRBflNZOKBdejveNtR/i5UFK2TYuldThJlpjnsZitpK2imE+JRvnTpl4MXt54OXz3E3eLlWqGztLZhPMZWvX8qaOU+FuZXOmR2X8/NknXsz2JMLTh8EzN3nxkwjxkf+ETBlEuzzZtb6CDOwDbhRXS1YzhsSLue1ZmJhC9pLhShByZr0p18gmzpCUfatKrWINLBNezJzxLBrEqzHXSO/bwFfyQfmTtPImswgP2rH1y0PJAKu0TAwR8xbyfMl1DvnsrYzfKOn7tiolygDWieewTXLmVljGMjF0zFwHeXIKV488K2qJJWHRMdf6X3Qw87DomGv9LzqYeRgzZpbeHpE2+Q6tqCWWhLFiZpIdzxdDK2qJJWGsmB+W9PO4ZN5o1Pq3ROjL0nhFTi1tcQ6NvT2MH/dq/VsilCUog89TS1ucQ2P9tA5gSOOBiTFWnLdI+okXHRr7HyuwXMaKcyf5qbT+Yteh8cDEGDNOpjG8Ewk3QTX233gggh1U7FB4T7IxEflM3glyaBYdZ2P/swJjHS28iDfJEn/Oi/NZWD9NDB2n1V+HxgOC1V1bO2NPy2WS/X0sAyGfyftSUoYdV+E+lT4ZMs6UaRRt1bADMXtL2472gGzbQcoxFiopSx227/bNkHF606Y4bf2zLbmGHQjhld4Lkvz7V9OzoIxtAXmOjJ6ZQpxeDG4mm63J45Rv+4/G8KbO7lzHkNEjU4jTi8HNvFuSx3UjhPcDtiMUvZ2g1KEue477ZApxejG4mbZX+IAq9T/eW7B4J+hBknza6BPrL2TsOK2tGpbpyR0sJHwP2/T+la9H2MYYjhkne2VqeIXMpsAIahkGsO84GTy2LM8k5asRv8C2rwb7AodmsnHaj068i7O9yEbvBTYbL6nb903EY7Jx2t44pgec6l1hS5tND7r+gCaHScf5vKQDZu5dJ6i2HM7+wbGYbJw8nNsjEsG1TVT5j1pQvJDZQ47FpOM8QvLgTYdfy8slv+Dhrod8Jo9fHlEm9edbfTHpOFn6sefNNvkqDbmcNYvJx8nv3e6Q4ULlu/J22fmHyiOwLHEWCoVCoVAojMvKyn859yRSysIRgQAAAABJRU5ErkJggg==">
                                                    <div>
                                                        <h5>Dostawa lawetą</h5>
                                                    </div>
                                                </div>
                                            </li>
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANwSURBVHhe7Zw/rA1BGMWfiIREo1Gp6OjoqIiKTlRKUSlRKUVFhU4pGko6pT+FEhUVGg0SDQrCOdwvmWzu2zs7O/d37313fslJdndmvjNn7+7O7iveRqPRaDQaW5VtEzUGcli6Ib2fyNs+1ujhoHRNeiv92URucx/3bYj90lXplZSeqE/SLenoRN72sbSPx3isa6wV+6RL0kspPSGfpbvScWm71MXH3OY+7puOdS3XdO0tyV7povRU+i1F8G/SPemUtEPKxX09xmNdI+q5tj3sZc+VZo90Xnoi/ZIi5HfpgXRG2imNxTVcyzVdO3zsaW/PwXNZCXZL56RH0k8pwnjbx9zmPrkMfY2p7Y+wS/IV8FCqcQX49jwp3ZE+TuRtHxtym/fdAZ6r5+y5LwQHOS3Vegb5qjgr3Ze+SlGvK7e5j/sOuZJmPYOdZciPU4RXwROSV8EvUhrMq+Blacgq6FAXpMfSDymt90a6Lh2ZyNs+lvbxGI91jSE/lufouXbfApzJ2Zxx2ltAEX7+HJNuS933sNeS38MOSLm47xXpmZReCd72Mbf11Rs7vov7OoOzRC3LWZ3Z2Ud9Sn6Q0sLvJH8JHJJysHntKyjIvYJzT4AzOZszprV8DoqJIjclTyaH7iKQTqb0GTaLvmdoySLkrM4cNYrJLVA7wBhq/oAxrpi+ArVvoXkw9hESfYvpFqj9EKcZOv9oLyYKzGMRWDQ5d1DsF5MWtea1CCyavme4VYwHpw9iYhFYNM4YeSN7MbEIRMF1IfLGIjSadT2B1WgncCTtBI6kr2C0raqm0ddWRI7ZqmoafW1FoGYQaCbUDALNhJpBoJlQMwg0E2oGgWZCzSDQTKgZBJoJNYNAM+WYPf+3958X0rLv52SqRo6Z/yQeeHvZ93MyVQM1g0AzoWYQaCbUDALNhJpBoJlQMwg0E2oGgWZCzSDQTKgZBJopx6z2l8K893MyVSPHbNab/7Lt52SqBmoGgWZCzSDQTKgZBJoJNYNAM6FmEGgm1AwCzYSaQaCZUDMINFOOWe0vhXnv52SqRo7ZrDf/ZdvPyVQN1AwCzYSaQaCZUDMINBNqBoFmosy8Uka9zZQuBmOIetPoayuCMktXx83k/3tQg6g3jb62IlAzCDQTagaBZoqC66ZqTCu+Dmo0Go1Go9FoNBr12dj4C2XuwF0a67cTAAAAAElFTkSuQmCC">
                                                    <div>
                                                        <h5>Odbiór w salonie</h5>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 1 -->

                            <div class="tab-pane" id="step2">
                                <div class="row p-b-15  ">
                                    <h4 class="info-text"> Wybierz sposób płatności</h4>
                                    <div  class="col-md-6 col-md-offset-3">
                                        <ul style=" padding: 0; margin: 0;">
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAq8SURBVHhe7dwFrCRdEYbhxd3d3T0Q3C0Ed3d3dwj6E9wdghMcgrsFhwDB3d3d/Xs2W8mh06dn5u5c6fvfL3mzO+d2z0zXHKmqU9379rSnPR2kDhfOH24T7hfuFq4bThv2NKHjh8eFn4f/dvhCYNjDhz01un74TRgz2hhfCmcLe4oeFP4TyjifDLcNZw9HC8cK5wqG87dCHfeHcPlwqJbhWAb5bdATp3TEwOD/Cs75XWDoXS3z1YXCjcI9gp5063CV8KdQhjhTWFbXDGXEL4ZdOSeeO7wk/DpUL+tx97CqHhzqfD/IrpHV9KXh36E10hTmwJeF44ZldaTwneD8z2vYDTIffTuUYf4eXh1uEAxRi8JRw2kOtL0y/CPU8T8M5wjL6oGhzvWes5YLN5fVBb0unD4s0inDm0OdZzFZ1oimiTrv2hrmqhOEcjFM7vcJrY4TXOA9w7U0jOjeodyZH4RlhrP3LQNuZB7dMbJY1IW0xjtheF5ohyl6TvB9Qx3zcg0LdJRQx/sBZilObi0Yr9dwQIz0/VAXWHw5cI7HdJhQw1lvPG+Yknm13vfmGuao6n0WjNNpiKzEFoS6uDeGSwaLyCKdIVSPfbGGCd0u1GecR8PcxIEtP89qWzJs68JEDqvqDcG5wrUjaOjo48FxPw167+x0kVCG4pbQsUP1IMNxFR0jXCC8MNT7PjxcNpwqtEaS4qpjHqphjrphqIs4o4bIaltthu0iMfidwgfDIufbnPrkcL1QLtMvg9V4luKW1MXpPdS2Tc15Rw8PCfy+On5VLDRXDbMVl6UuhktB1QMF+T0Zpm1qCny/ZwZJBhGNhUi4dupw4XDX8N7wz1DnMOATguNmqVuFuhgXWuLC9FwVgX9rBIa+Ylh2EeC0Py1Y9es9LCbHC7PTBUNdxHU0LJBeVNEGA9w5HDaMyb7IHUOvdwkTPxHq8/mXjDsrSXD+ObiA1o0Z09VDGc/Ef/EwpV8FxzJ6T0cOIpYy4seC7zQrvSj48npUO4xbcbBr1ZREXSZZUEZ59P5XfRn6rRGfEmal84XqWSKOMb01+LvjrqahI3/7QKhMNSQnvheeFHrznAWshjNXyNQyK7XJhGFQf7lQf3u+ho4OCXVcDyt1L0Wml5cDz6eclfSMyg7rZbIqtaq+LWg3V55Uw4i4LmUkQ/254TEHeHz4cKi/fyr0Vmyrcx1nH2ZWMq/VxA/DViRSmz9PDT0Zto5hvKpA4AZV2ovBnh7qvXsL0IlCuUjP1jA3cYDHUli4aOip5jxJiJLNc22VadZ7673s7vX0/uCYn4VlfcsdJbHtcFNJuUbP36MxA9a57So8NOqYuD11bqXXZqlzhl8EF/JuDROqXtMO4TJCa8B2WPfUZoiuoWHOkqdzIXzFKY0tIvXa/CjbbH5bRnbn6lxZnlmr4tXH7n81rUeGuvAx/hIkLxbNa0K/Ouf+GuasCvO4F8voysFwlokuIwy5V5iSLE4d225wzVI/Di7EHvFGJbaVka7VXU+cGs7m3jKgbNGsJSJwIZ/d/6ovWZfXhtbRHra1Ec1NNXQkwVrHXUzDnCWKcCFcmqleo1c5rt3bqPlTERHJYJdhpuY2DrRjONTH1DBnXSHURU9VUEkWOEbIVotEnVdujGLKauv1QOfWtPFRDXOXLcmqef506K2gMi1lnGcEw7ZePzEYvsvMgbI5dZ5k7a7Qo0JdlK3IMUlGtBvxU/RWYXvUstKO4UuKiHaFuBUuyIUpfevtlQi7ZFpaY7XoeVP1L+LjOvZhGnaT7hLq4t4R3AsyJkNcpqU1hkjEnKdQqadLh8oF+pHUIO4qMQzDlVEkVntGLFUyQoQyJTUx9lgcy4i2QHelzHNulCkjvjP0hjO9IkhG2J3ryU5gm/6/Q9jVOlmoiR6Gm5qa3urck6pW9TO1F+PfqRzhtkvZhjSSkIqn75c375j4p/J8Y1J5WptMBRfHzTUnCT0Z8jLc4upyvPH70Kt63RL5YicP8miKhdzHwRd7S1D9vqh25Y/BbpyU07KTtx4nVBu+t3nvM0F5G0O5d86G1XtCu11QvD1s2U2IvvSlAr/MLQW8fhFAW2ZxsDCIyGHZ3J150SIxdYPhEEaWoDUatkz2TfWosS80BkPofer79Ea7bOYqwbmyW3sf3A23apmL7D+05/81KBRa9tYDTrBoQ497X/hRsDgwlgLOzwW3RFgkzH9brjYasNTreR8KeqK/KZVV4GOeqzK1VWQakD5vtx+hd6samP39bLVnK8CW1dhM2X2zF1wrJPxfb57dXm2pLqbdoOFfWTCKKX9rI1K9z5erfeFCjlCWZlaqL98asHbMCq83Q1yd54S/hfbzJFmV6qqw2moJC08Rlq7kqi/dGlDvqHDJv15vpk4cbC7x2+r7wP6HfRD3mHBLFAl9Iyi8fFUwt67qaw6lnoaf6f3aFd9n28NeuDDVCa0Bt0vSTG59WMV1seBNOdlDySfeJNhO7VVKtPA6+MFd1YE7wYAl249XCi8IXBfRiHSWGPk1gXPeOs/+3nPU+ZOqFbhOXwt1zhA/GndIduiWQY+s9UFCopvxqTfYSQZcRgzW3jOiqp94EtwuBebm0pqKhpgurP4enaL4aSzmFkHV8fzQUdUBrQFlQkQmPHr/TmVGtlN8TKW7vr+h9pHQi57EyCr53UfsToBFKbNSVYfpvaOqD2gNWIU6xdRtCdutW4T2uxYMybdVnKkTbHRF1/O8nwhqVPWBczWgLEx9T+kwG02qGDYSNY2pKmxVwI6qPnwrhrAVUCWCPeEza1iD2rqZs2hYo7gwMkreu71d9/80ZsDNkjvH6/OscCIPiYhV7ybi+1WtTL2fUbNq8nVKws5vhvqu3UqG+gJbYUA9kCtS7kHBTbBqLupBEq63D0N3RKS07LMUFsk+yjA7NWmbpQ5as3j/vYeJ+dWfFWxTciOkqRSUvyvU7lphS9R0sI67kPx4fMz2x7Vy8wsne3Yd3BpQ6FZv5N/NCuUMXTGvoTjslVN8N5gO1rFQyFpbKNrEhtobMbp9moWqk1oDLkomuHDxK1adv3qSWLD5o6fVxF1wIb4aVPObiw42/iXGYaQqVAIjMuZKWwF1cmtA6Sv+k6HjX5tFJnvltSbr1ln1f1uShuRZw7ok0pAVWSXOXUaGuxuz280nvd/w3dAqXr+2OLDkS5fBpuLHIb6I5X6dhlyXJCpk2Ie924JxUA+lqIc4QLro683rIfYi7ILpbSZ3K6L/S9e3c5j401M1ejcYbqXExjI8w10+SYq1VCrY3KmK+SEMZk4SP0rpTD0lw3NaBPft8Da/2IJcdidunbJZZk4dzudi58uEtcqQVYenJ70pPCD4daYM1pMIQ/lt2yMNG8NnK8rKZJKNDLt3reHs3nG+ZyNbnHpvexGehWqPpZ6hsE7JrqjUqk2ywuqtWmKdUcqW6hKhUk7FT4I5dB13kDMMAzFU+xkMebOwbNpqx8sdR20FFhQQ3Ths1K8zJA3N9j0NXUN4drf3LyOG8rzU4WNNGHaVZ7xYBIa92mLhuTSbMT3sOFmc9BJDuTUCoxiOY9GNcxSKtxkYcE/c7rDZRQE7UnqL2prhw2i9lrXhyLu3QzXD0B2plX22j3Zap7g3jwhDZ3cMvimXa6ou+lAr2RUpLGHlVwK3B8JJvqUN8G26RWHfvv8BABdxrSTCXGUAAAAASUVORK5CYII=">
                                                    <div>
                                                        <h5>Gotówka</h5>
                                                    </div>
                                                </div>
                                            </li>
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIOSURBVHhe7ZxRTsMwEER7DTgScBwQ/HIv4Ae4EOIDvmAHsBq1VtYTOylrz5NGqpXuVjOJKq2TdieEEEIIIYQQQgjRkivTo+nd9NWZ4AneLk2rcG/KfXCPgtem4MpD4w/TtenM1BvwdGP6NMFr0yvxyYSmCK93bk3wCs/NeDOhaY9X3iHwCK/w3Aw0hEahuV8FWIkCrCQ1HE3NyDUfQc3INR9Bzcg1H0HNaN7wn6MAK1GAlSS/U1Xt1qQmozANLid6tyYVjkLOb9VujQLcs2i3RgHuWbRbk2v4anr5ffnD2ustmQsQeMePyBU8/ymx9npLvIC840fQBcHx/NJ50AXBSX49FUMXBGca0pyKoQuCMw1pTsXQBcGZhjSnYuiC4Hh+6TzoguB4fuk86ILgeH7pPOiC4Hh+6TzoguB4fuk86ILgeH7pPOiC4Hh+6TzoguB4fuk86ILgeH7pPHIFrff7Tr2e4gXkHT8iV+Dt30VbT/EC8o4fMdIDlucmeJ3bsqcDTI/44q5U79yZ4HXuphEdYHrIHLf0ECLOUm/AE8IruW1JBwj0M4c96X00OCsPpvSd2JPgCd4uTB6ppjmrNf5nKMBKFGAlCrASBVjJpgG2nllPvQabBujNoNHWYLUAR5iZS2blxYwwM5fMyovpeWZmZuUqRpiZm/8lwCE9zszMrCyEEEIIIYQQQghRwm73DTrZvdOedlupAAAAAElFTkSuQmCC">
                                                    <div>
                                                        <h5>Karta</h5>
                                                    </div>
                                                </div>
                                            </li>
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAANmSURBVHhe7Zy5ixRREMZHFDwwUAQNvMAD1EDwDMRAxMREMDDzbzATjAURA1FBUBDBC8Qz8AgMjNRQQ0HBSETwvgPv7xv3g2Lp6XX7vZqeeVs/+MG+2p0qXjFT3dMz250gCIIgCIIgCIKBZyG8Cj/BP4XJPV2Dy6ELC+BbWFW8JN/A+TA7VyAL3IAuBVqGe7oFucdLDOTmM2RyPhNLhXvjHj92V5lhYlo6bvuMBiYSDUykLvEDeP/fj12GbW1ppYH3RhTDtra00sCSiAYm0koD62bKIDLwM3DQiRnoSDQwkVYaGDPwP6hL7DUDd404o7vKx4SZgaq5qLvqD277dEvcA16fU82NDPQJt33WJc41A9fCs7Dqyvd7eA6ugakUNwMnwUPwN1SdXvJvDkI+pinFzcD9UPkpm/TUrF+anyUf44HyZ8cr8RL4Ayo/X176ZEwxHkTWw0cmxsesgLlR/uzUJU6Zgfugcr+Gc6BQXEdh/u4FVPwAbEJRM/AMVO7rDBgUt6cxx6Di/BStCUXNwItQue8yYFA8zgNrsAeQn3ArFIoX38CUGbga2tOX7/AIXGdiuRtY3HngCaj8VR6F2+E0mIOiZiCZDI/DsU6kv8LTcCn0QrWy45bYsAlehmyU6lX5De6GHqhGduoSp8zAKqbDbVA1q94b84CzBTahuBnYC9VcDHlQ0benJBvRhOJmYC9U0x6FL0DFf8FZMCfKnR23xDWopm3gBqg4XQlzorzZqUvcdAbyooHyUl4PtChuG8iG2cfw5T1eipmBU6E94p6CFsVtA/dAxXlwmQLHS1Ez8DxUbnoYzoREMTVwJ/wCFT8Jc6Pc2fFKzG/+v4LKT/l14jtmfRM+MWv6HM6DuVH+7NQlTj0P5Ox7BlVjLHm1ehVsSpHngXzZ7oWPoWpZ+TbvIeQM5Il2CkXNwCrmws1QNXfA2bAfuO3TLXENqmmPwt647bMuceoM7AXfD9Ncl7FEkTOwnxQ/A9skGphIKw30moFexAxMJGagI9HARFpp4OiZMmxrSysNHD1Thm1taaWBJRENTCQamIjbPnWvmJJvOsGr49yjy00neFMaJufl9RKbyObdhtwjby6UHX4EORFuvPMO8nvbLvCfX3hTGj7Fq4oPsx8gn3nLYBAEQRAEQRAEwcDS6fwFlz4Xd2NhoSgAAAAASUVORK5CYII=">
                                                    <div>
                                                        <h5>Raty</h5>
                                                    </div>
                                                </div>
                                            </li>
                                            <li style="display: inline-block;">
                                                <div style="border-radius: 20px; border: solid black;padding: 5px;">
                                                    <img style=" margin-left: auto;
    margin-right: auto; display: block;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAZ3SURBVHhe7Zx1iHxVFMd/dnd3YCAWNnZgd2CLf6h/iAG2YmCAmNjYXYiB3Qh2oNii2K3Y3fX5rHvg8nizM7tvZva93fuFD7vz5s1795259557zz13JmRlZWVlZWVlZWVl1Vfzw0HwAHwO3wz+vzVMBFklWhAOhWfgH/i3Bc/BJpCFFoHDQaOkRvoJboBtYE6YFfaBjyHOeQzWgnGnxeFIeB5So/0A18G2MBWUyeM27S8hPmfTXgnGtJaAY+AlSI32LVwDW8KU0KmmBa/n572OTf42WArGjHyY4+BVSI2mQ7gSNoMpoIpmgpPgR/Daf8O1sCg0WndCarSv4FKw858Muq054Cz4Dbzfn3AiNFZhuItgQ5gU+iGHPxeDBvT+h0AjFQYcLemI7Bf/gs090DR1y4BTw1HwBXwKV8CuMDu0k5+zDN/B0h5okrphQB3EyxDXSrF2OQw6FdaHMg/ujMVhkee/BbNBYxQPWkWXgNd4E1YFvfqBcC/8DHEPceC9JxRlDX4aPOdhmBwaoXiwKvoavEbZ2M4atx6cDNbE6O92gaLmgY/Aa/mlNELdMOBwrnEEeO7voGGLWhGi1h7ggbprOA/fSnGNlF/hM3gFrgeDD07jJoYzwHN0GitAUTtA1NSNPVBnxQNXkcGCuE47NKoD6RcGX+u1DVQUdSz4vkZezAN1VTxYt2XfZ3TGJrkH2KdF/1ZEz1sc7uiZbwLfv8UDdVU8RL9kMz4PjOjEvcVh0AyQaj7wve8HXtVU8QD91sxgqCwNdz0E6fAlDGizr62i8AZLO5k1dFszgnHCKMc9YMRb4909eMwwWm2VOgAjJHrMNaCfWgiiDGWUeerayGHFVmAtMEYXhbZjd/C7CvR6cSiaqrgUYBzSZvvJ4DHjlI3QwmBsLgoe6D0vhN1gSZgOjBU6B7a5LQNbwH7gGM8pnNM6p23Waocqr8P9cAHsD2uD/WDaVONeIefEcbxRmgR8wHOg1dCjV3wIqeJ4Y2XzXQ0MORm9tmlbs/4Am9p7YBOzFp0POqPtwX7LGupSgM5pWbCrCIM8Cw5PvMbt4Bel8TaCVI03YLc1XINkAxYUBukk5tfYPrCXipU//w5lRN9Lz80aVGqYlFDxeDtDj0uVGTGUHsvG60CtDJjVobIBKyobsKKyASsqG7CisgErKhuworIBKyoM5oBZsgGHqbJpnceyOlRxWtdu+uYShBn/Ls57rrFJF99dFnXxyWi3a9G9yK5trDTamnAumHuY1tZW/AKPwulgEqeJS+NKGs3VwbOhuEbzDpiwvjJMAy7ILwd7g0nxb4C5Neln5AO4EYyMj0lptNXB5plu1JF34RTodInTBSsT5Y+H+8BmHtcy+2vMKNZczoTiApbrLWa22qdVlV+Oe2Di2qMilyBvBvNaTMlwEd5FIAs3XNnJ7wsaKTXa+3Aa2Dx7sR49agacFyITNcW+xpW6g8Fm04lcqTOFI65hqpudfT8W8eOefVekmt0BerQFwD7GDj0Kpee7DNo1OZMtPd/aVpal2ktFWfuu2KJlTUxl8zUrwWyENDXErbC7Q1lWfmT0rzvwanh6Ah7//98RKcrXd7kA7o3nGnhVLjNNbYppU7ev1BGYQBSKbV6dblpMjWYylIxUUa6+6yrwxmYgmHEwlNzqalaq2QdRYGunn90UwnEMVQO7abRUUZ6+y/SM6O8eAT3lULUxpDd1cGvSeRQ+cGCrF3dLrKkfh4FTMdVNo6WKe4+K9LLmscQo36ZotlXaPFtpFtB5OEuwO9CBxMOkmHPTadMeieI+oyqnTU6Jwmm449KJvKlvncpdoQ593gb3jFgb3V/nnLeXqoUBQ/4kwOVgrbFQGvRW6Ne2fseM7o5yQO7vNhhoMMPLL2JHKBuX1sqAIff+GilxHBgFNON1HeimzFtcHtzB5BeVJqiX4UadJ8GfHIhNPvFeLaWjMUpi3l8U9ClwnDiSGYbTPTcv6lzugvS6gTXuatgLbBFul7WvNePfriE910zZ+L/WMvP+aEgL7I9Y7AzWolbSeRgstcY8CAZL4/OB00a7DQfpphcPJb27oSsdXTrn1vk1QsbqbG5plEUDWFv0ytPDBnACGPyMAXagt38NNIC7O+eGKnKgb39pgLVRcvOMRtN4qYGK6IReBIOo28Fo7FuptWy+O4HN0/7MIKdzZqd/9pPtZjhZWVlZWVlZWVlZWVljWhMm/AdUBoyj8GXOOgAAAABJRU5ErkJggg==">
                                                    <div>
                                                        <h5>Leasing</h5>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- End step 2 -->

                            <div class="tab-pane" id="step3">
                                <!--<h4 class="info-text"> How much your Property is Beautiful ? </h4>-->
                                <!--<div class="row">-->
                                <!--<div class="col-sm-12">-->
                                <!--<div class="col-sm-12">-->
                                <!--<div class="form-group">-->
                                <!--<label>Property Description :</label>-->
                                <!--<textarea name="discrition" class="form-control" ></textarea>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->

                                <!--<div class="col-sm-12">-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<label>Property State :</label>-->
                                <!--<select id="lunchBegins" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your city">-->
                                <!--<option>Seoul</option>-->
                                <!--<option>Paris</option>-->
                                <!--<option>Casablanca</option>-->
                                <!--<option>Tokyo</option>-->
                                <!--<option>Marraekch</option>-->
                                <!--<option>kyoto , shibua</option>-->
                                <!--</select>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<label>Property City :</label>-->
                                <!--<select id="lunchBegins" class="selectpicker" data-live-search="true" data-live-search-style="begins" title="Select your city">-->
                                <!--<option>New york, CA</option>-->
                                <!--<option>Paris</option>-->
                                <!--<option>Casablanca</option>-->
                                <!--<option>Tokyo</option>-->
                                <!--<option>Marraekch</option>-->
                                <!--<option>kyoto , shibua</option>-->
                                <!--</select>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<label>Property Statue  :</label>-->
                                <!--<select id="basic" class="selectpicker show-tick form-control">-->
                                <!--<option> -Status- </option>-->
                                <!--<option>Rent </option>-->
                                <!--<option>Boy</option>-->
                                <!--<option>used</option>-->

                                <!--</select>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<label>Property Statue  :</label>-->
                                <!--<select id="basic" class="selectpicker show-tick form-control">-->
                                <!--<option> -Status- </option>-->
                                <!--<option>Rent </option>-->
                                <!--<option>Boy</option>-->
                                <!--<option>used</option>-->

                                <!--</select>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-12 padding-top-15">-->
                                <!--<div class="col-sm-4">-->
                                <!--<div class="form-group">-->
                                <!--<label for="property-geo">Min bed :</label>-->
                                <!--<input type="text" class="span2" value="" data-slider-min="0"-->
                                <!--data-slider-max="600" data-slider-step="5"-->
                                <!--data-slider-value="[250,450]" id="min-bed" ><br />-->
                                <!--<b class="pull-left color">1</b>-->
                                <!--<b class="pull-right color">120</b>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-4">-->

                                <!--<div class="form-group">-->
                                <!--<label for="price-range">Min baths :</label>-->
                                <!--<input type="text" class="span2" value="" data-slider-min="0"-->
                                <!--data-slider-max="600" data-slider-step="5"-->
                                <!--data-slider-value="[250,450]" id="min-baths" ><br />-->
                                <!--<b class="pull-left color">1</b>-->
                                <!--<b class="pull-right color">120</b>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-4">-->

                                <!--<div class="form-group">-->
                                <!--<label for="property-geo">Property geo (m2) :</label>-->
                                <!--<input type="text" class="span2" value="" data-slider-min="0"-->
                                <!--data-slider-max="600" data-slider-step="5"-->
                                <!--data-slider-value="[50,450]" id="property-geo" ><br />-->
                                <!--<b class="pull-left color">40m</b>-->
                                <!--<b class="pull-right color">12000m</b>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-12 padding-top-15">-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Swimming Pool-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> 2 Stories-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Emergency Exit-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Fire Place-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-12 padding-bottom-15">-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Laundry Room-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Jog Path-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Ceilings-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<div class="col-sm-3">-->
                                <!--<div class="form-group">-->
                                <!--<div class="checkbox">-->
                                <!--<label>-->
                                <!--<input type="checkbox"> Dual Sinks-->
                                <!--</label>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--</div>-->
                                <!--<br>-->
                                <!--</div>-->
                                <h4 class="info-text">Twoje dane dane </h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>W tym miejscu wyświetlą sie dane użytkownika</label>
                                        </div>

                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"/> <strong>Zamówienie na inne dane</strong>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 3 -->


                            <div class="tab-pane" id="step4">
                                <h4 class="info-text"> Podsumowanie </h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="">
                                            <p>
                                                <label><strong>Potwierdzenie danych</strong></label>
                                                Jeżeli zgadzasz się na zakup wybranego przez siebie samochodu oraz Twoje dane osobowe i adres zamieszkania są zgodne z faktycznymi to możesz przejść do płatności. Zanim jednak to zrobisz, musisz zaakceptować warunki umowy zakupu.
                                            </p>

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" /> <strong>Akceptuję warunki umowy</strong>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  End step 4 -->

                        </div>

                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-primary' name='next' value='Dalej' />
                                <input type='button' class='btn btn-finish btn-primary ' name='finish' value='Zakończ i zapłać' />
                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-default' name='previous' value='Wróć' />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                <!-- End submit form -->
            </div>
        </div>
    </div>
</div>

</div>

<jsp:include page="/footer" />

<script src="/resources/assets/js/modernizr-2.6.2.min.js"></script>
<script src="/resources/assets/js/jquery-1.10.2.min.js"></script>
<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="/resources/assets/js/bootstrap-select.min.js"></script>
<script src="/resources/assets/js/bootstrap-hover-dropdown.js"></script>
<script src="/resources/assets/js/easypiechart.min.js"></script>
<script src="/resources/assets/js/jquery.easypiechart.min.js"></script>
<script src="/resources/assets/js/owl.carousel.min.js"></script>
<script src="/resources/assets/js/wow.js"></script>
<script src="/resources/assets/js/icheck.min.js"></script>

<script src="/resources/assets/js/price-range.js"></script>
<script src="/resources/assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
<script src="/resources/assets/js/jquery.validate.min.js"></script>
<script src="/resources/assets/js/wizard.js"></script>

<script src="/resources/assets/js/main.js"></script>


</body>
</html>