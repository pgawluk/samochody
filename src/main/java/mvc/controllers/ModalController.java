package mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller("modalController")
@RequestMapping("/modal")
public class ModalController {

    /**
     * kontroler wyswietla modal z alertem ze cos sie nie powiodlo (np. email istnieje juz w bazie i nie udalo sie edytowac/zalozyc konta)
     */
    @RequestMapping("/alert/dbError")
    public String showError(Model model, @SessionAttribute("dbMessage") String dbMessage){
        model.addAttribute("alertTitle", "Błąd!");
        model.addAttribute("alertContent", dbMessage);

        return "modals/alert";
    }


    /**
     * kontroler wyswietla modal z alertem ze logowanie sie nie powiodlo
     */
    @RequestMapping("/alert/wrongCredentials")
    public String showError(Model model){
        model.addAttribute("alertTitle", "Błąd!");
        model.addAttribute("alertContent", "Podane login i hasło są niepoprawne");

        return "modals/alert";
    }
}
