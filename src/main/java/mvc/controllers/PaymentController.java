package mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by Przemyslaw Gawluk on 14.12.2017.
 */
@Controller("paymentController")
public class PaymentController {

    @RequestMapping("/payment")
    public String get(HttpSession session){
        return "payment";
    }
}
