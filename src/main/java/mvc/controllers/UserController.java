package mvc.controllers;


import mvc.enums.Role;
import mvc.helpers.ServiceResult;
import mvc.models.UserModel;
import mvc.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

import static java.lang.System.out;

@SessionAttributes(types = UserModel.class)     //potrzebne zeby przesylac obiekty miedzy kontrolerami gdy jest sesja
@Controller("userController")
@RequestMapping("/user")
public class UserController {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    IUserService userService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;



    /**
     * kontroler przenosi do widoku logowania
     */
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String returnLogInSite(Model model, @RequestParam(value = "wrongCredentials", required = false) String wrongCredentials){

        log.info("Wyswietl strone logowania");

        if(wrongCredentials != null){
            model.addAttribute("wrongCredentials", "Login i hasło są nieprawidłowe");
        }

        return "login";
    }


    /**
     * kontroler przenosi do widoku rejestracji konta
     */
    @RequestMapping(value="/register", method = RequestMethod.GET)
    public String returnRegisterSite(Model model){
        UserModel user = new UserModel();

        model.addAttribute("user", user);

        log.info("Wyswietl strone rejestracji");

        return "register";
    }



    /**
     * kontroler wywoluje serwis zapisujacy uzytkownika do bazy
     */
    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("user") @Valid UserModel user, BindingResult bindingResult,
                             HttpSession session, Model model){
        if(bindingResult.hasErrors()){
            log.info("Rejestracja konta - wprowadzono niepoprawne dane, zwroc formularz");
            return "register";
        } else {
            ServiceResult<UserModel> result;

            log.info("Rejestracja konta - dane poprawne, wywolaj serwis zapisujacy do bazy");

            result = userService.createUser(
                    user.getEmail(),
                    user.getPassword(),
                    user.getFirstname(),
                    user.getLastname(),
                    Role.ROLE_USER
            );

            if(result.isValid()){
                log.info("Rejestracja konta - uzytkownik zapisany do bazy");
                return "redirect:/?registerSuccess";
                // TODO: 2017-12-07 obsluzyc to - np wyswietlic modala ze sie udalo
            } else {
                log.info("Rejestracja konta - nie udalo sie zapisac uzytkownika do bazy");

                // dp wyswietlenia modala ze nie udalo sie zapisac bo email juz istnieje
                session.setAttribute("dbMessage", result.errorsToString());
                model.addAttribute("dbError", true);

                return "register";
            }
        }
    }


    /**
     * kontroler przenosi do widoku konta uzytkownika
     */
    @RequestMapping(value="/account", method = RequestMethod.GET)
    public String returnUserAccountSite(Model model, HttpSession session){

        log.info("Wyswietl strone z profilem uzytkownika: " + session.getAttribute("userFromSession"));

        UserModel user = (UserModel) session.getAttribute("userFromSession");
        model.addAttribute("userProfile", user);

        return "userProfile";
    }


    /**
     * kontroler wywoluje serwis zapisujacy edytowanego uzytkownika do bazy
     */
    @RequestMapping(value="/account/edit", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("userProfile") @Valid UserModel user, BindingResult bindingResult,
                           Model model, HttpSession session){
        if(bindingResult.hasErrors()){
            log.info("Edycja konta - wprowadzono niepoprawne dane, zwroc formularz");
            return "userProfile";
        } else {
            ServiceResult<UserModel> result;

            log.info("Edycja konta - dane poprawne, wywolaj serwis zapisujacy do bazy");

            result = userService.editUser(user);

            if(result.isValid()){
                log.info("Edycja konta - uzytkownik zapisany do bazy");
                return "redirect:/user/account?editSuccess";
                // TODO: 2017-12-07 obsluzyc to - np wyswietlic modala ze sie udalo
            } else {
                log.info("Edycja konta - nie udalo sie zapisac uzytkownika do bazy");

                user = userService.getUserById(user.getUserId()).getData();
                session.setAttribute("userFromSession", user);

                // dp wyswietlenia modala ze nie udalo sie zapisac bo email juz istnieje
                session.setAttribute("dbMessage", result.errorsToString());
                model.addAttribute("dbError", true);

                return "userProfile";
            }
        }
    }
}
