package mvc.controllers;

import mvc.models.CarModel;
import mvc.services.ICarService;
import mvc.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/managers")
public class ManagerController {

    @Autowired
    IUserService userService;

    @Autowired
    ICarService carService;

    @RequestMapping(value = "/boughtCars", method = RequestMethod.GET)
    public String showBoughtCarsList(Model model){


        List<CarModel> carsList = carService.getSoldCars().getData();


        model.addAttribute("availableCarsList", carsList);

        return "carsList";
    }
}
