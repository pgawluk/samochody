package mvc.controllers;

import mvc.models.CarModel;
import mvc.models.UserModel;
import mvc.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    IUserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String returnHomeSite(Principal principal, HttpSession session){

        if(principal != null) {
            String email = principal.getName();
            UserModel user = userService.getUser(email).getData();

            session.setAttribute("userFromSession", user);

            log.info("Wyswietlam strone glowna dla uzytkownika: " + session.getAttribute("userFromSession"));
        }

        if(session.getAttribute("basket") == null){
            session.setAttribute("basket", new ArrayList<CarModel>());
        }

        log.info("Wyswietlam strone glowna");

        return "home";
    }
}
