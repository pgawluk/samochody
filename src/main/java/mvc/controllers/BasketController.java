package mvc.controllers;

import com.google.gson.Gson;
import mvc.models.CarModel;
import mvc.models.UserModel;
import mvc.services.CarService;
import mvc.services.ICarService;
import mvc.services.IUserService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Przemyslaw Gawluk on 13.12.2017.
 */
@Transactional
@Controller("basketController")
public class BasketController {

    @Autowired
    IUserService userService;

    @Autowired
    ICarService carService;

    @RequestMapping(value = "/basket", method = RequestMethod.GET)
    public String show(Model model){
        return "basket";
    }

    @RequestMapping(value = "/basket/fetch", method = RequestMethod.GET)
    public void fetch(HttpServletResponse response, HttpSession session) throws IOException {

//        todo - dodawanie pojazdow do koszyka w szczegółach auta
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(session.getAttribute("basket"));
        BufferedWriter writer = new BufferedWriter(response.getWriter());
        writer.write(jsonResponse);
        writer.close();
    }

    @RequestMapping(value = "/basket/remove", method = RequestMethod.GET)
    public void remove(@RequestParam("id") Long carId, HttpSession session){
        List<CarModel> carModels = (List<CarModel>) session.getAttribute("basket");
        for(int i = 0; i<carModels.size(); i++){
            CarModel car = carModels.get(i);
            if(car.getCarId() == carId){
                carModels.remove(i);
                return;
            }
        }
        session.setAttribute("basket", carModels);
    }

    @RequestMapping(value="/basket/add", method=RequestMethod.GET)
    public void add(@RequestParam("id") Long carId, HttpSession session){
        List<CarModel> carModels = (List<CarModel>) session.getAttribute("basket");
        CarModel carModel = carService.getCar(carId).getData();
        carModels.add(carModel);
        session.setAttribute("basket", carModels);
    }
}
