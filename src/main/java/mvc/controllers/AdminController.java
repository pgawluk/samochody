package mvc.controllers;

import mvc.enums.Role;
import mvc.helpers.ServiceResult;
import mvc.models.UserModel;
import mvc.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;

import static java.lang.System.out;


@Controller
public class AdminController {

    @Autowired
    IUserService userService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping("/employee/addEmployee")
    public String addEmployee( Model model){
        UserModel userModel = new UserModel();
        model.addAttribute("employee", userModel);

        return "registerEmployee";
    }

    @RequestMapping(value="/employee/register", method = RequestMethod.POST)
    public String createUser(@ModelAttribute("employee") @Valid UserModel user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "registerEmployee";
        } else {
            ServiceResult<UserModel> result;

            result = userService.createUser(
                    user.getEmail(),
                    user.getPassword(),
                    user.getFirstname(),
                    user.getLastname(),
                    user.getRole()
            );

            if(result.isValid()){
                return "redirect:/?registerSuccess";
            } else {
                return "registerEmployee";
            }
        }
    }

    @RequestMapping("/employee/employeesList")
    public ModelAndView employeeList(){

        ModelAndView modelAndView = new ModelAndView("employeesList");

        ArrayList<UserModel> list = new ArrayList<>();
        list.addAll(userService.getUsersList(Role.ROLE_MANAGER));
        list.addAll(userService.getUsersList(Role.ROLE_ADMIN));
        modelAndView.addObject("list", list );


        return modelAndView;
    }

    @RequestMapping("editAccount/{userId}")
    public String editUserAccount(@PathVariable Long userId, Model model, HttpSession session){

        ServiceResult<UserModel> result = userService.getUserById(userId);
        UserModel userModel = result.getData();
        model.addAttribute("userProfileAdmin", userModel);
        session.setAttribute("userToEdit", userModel);

        return "userProfileForAdmin";
    }

    @RequestMapping("editAccount/account/edit")
    public String editAccount( @ModelAttribute("userProfileAdmin") @Valid UserModel userModel, BindingResult bindingResult, Model model, HttpSession session){

        UserModel user = (UserModel) session.getAttribute("userToEdit");

        if(!user.getEmail().equals(userModel.getEmail()))
            user.setEmail(userModel.getEmail());
        if(!user.getPassword().equals(userModel.getPassword()))
            user.setPassword(userModel.getPassword());

        ServiceResult<UserModel> result;
        result = userService.editUser(user);

        if(result.isValid()){
            return "redirect:/";
        }

        return "userProfileForAdmin";
    }

    @RequestMapping("/deleteAccount/{userId}/{role}")
    public String deleteUserAccount(@PathVariable Long userId, @PathVariable Role role){

        out.println(role);
        userService.deleteUser(userId);

        if(role == Role.ROLE_USER)
            return "redirect:/customersList";

        return "redirect:/employee/employeesList";

    }

    @RequestMapping("/customersList")
    public ModelAndView customersList(){
        ModelAndView modelAndView = new ModelAndView("clientsList");

        ArrayList<UserModel> list = new ArrayList<>();
        list.addAll(userService.getUsersList(Role.ROLE_USER));
        modelAndView.addObject("list", list);

        return  modelAndView;
    }

    @RequestMapping("/setActiviti/{userId}")
    public String setActiviti(@PathVariable Long userId){

        ServiceResult<UserModel> result = userService.getUserById(userId);
        UserModel userModel = result.getData();

        userModel.setIsActive(!userModel.getIsActive());
        userService.editUser(userModel);

        return "redirect:/customersList";
    }

}
