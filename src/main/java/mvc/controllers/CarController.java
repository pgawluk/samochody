package mvc.controllers;


import mvc.helpers.ServiceResult;
import mvc.models.CarModel;
import mvc.models.FileUploadForm;
import mvc.models.UserModel;
import mvc.services.ICarService;
import mvc.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static java.lang.System.out;
import static sun.plugin2.util.PojoUtil.toJson;

//@SessionAttributes(types = CarModel.class)     //potrzebne zeby przesylac obiekty miedzy kontrolerami gdy jest sesja
@Controller("carController")
@RequestMapping("/car")
public class CarController {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ICarService carService;

    @Autowired
    IUserService userService;

    @RequestMapping(value = "/carsList", method = RequestMethod.GET)
    public String showCarsListSite(Model model){

        log.info("Wyswietl liste samochodow");

        List<CarModel> carsList = carService.getCarsList().getData();

        model.addAttribute("availableCarsList", carsList);

        return "carsList";
    }


    @RequestMapping(value = "/addCar", method = RequestMethod.GET)
    public String display(){

        return "addCar";
    }

    @RequestMapping(value = "/addCar", method = RequestMethod.POST)
    public String add(){

        return "addCar";
    }

    @RequestMapping(value = "/addCarPhotos", method = RequestMethod.POST)
    public String addCar(@ModelAttribute("uploadForm") FileUploadForm uploadForm, HttpSession session) throws URISyntaxException, IOException {

        List<MultipartFile> files = uploadForm.getFiles();

        if(files != null && files.size()>0){
            for(MultipartFile file: files){

                String fileName = UUID.randomUUID().toString();
                URI uri = new URI("/photos/" + fileName + "." + file.getContentType());
                File localFile = new File(uri);
                file.transferTo(localFile);
            }
        }

        return "home";
    }

    @RequestMapping(value = "/uploadMyFile", method = RequestMethod.POST)
    @ResponseBody
    public String handleFileUpload(MultipartHttpServletRequest request)
            throws Exception {
        Iterator<String> itrator = request.getFileNames();
        MultipartFile multiFile = request.getFile(itrator.next());
        try {
            // just to show that we have actually received the file
            System.out.println("File Length:" + multiFile.getBytes().length);
            System.out.println("File Type:" + multiFile.getContentType());
            String fileName=multiFile.getOriginalFilename();
            System.out.println("File Name:" +fileName);
            String path=request.getServletContext().getRealPath("/");

            //making directories for our required path.
            byte[] bytes = multiFile.getBytes();
            File directory= new File(path+ "/uploads");
            directory.mkdirs();
            // saving the file
            File file=new File(directory.getAbsolutePath()+System.getProperty("file.separator")+"test");
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(file));
            stream.write(bytes);
            stream.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new Exception("Error while loading the file");
        }
        return toJson("File Uploaded successfully.");
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> uploadFile(@RequestParam("uploadfile") MultipartFile uploadfile, HttpSession session) {

        out.println("Upload File Controller");
        try {
            ArrayList<MultipartFile> multipartFiles = (ArrayList<MultipartFile>)session.getAttribute("image");

            multipartFiles.add(uploadfile);
            session.setAttribute("image",multipartFiles);

//            String filename = uploadfile.getOriginalFilename();
//            String directory = "/var/netgloo_blog/uploads";
//            String filepath = Paths.get(directory, filename).toString();
//            BufferedOutputStream stream =
//                    new BufferedOutputStream(new FileOutputStream(new File(filepath)));
//            stream.write(uploadfile.getBytes());
//            stream.close();

            out.println(uploadfile.getOriginalFilename());
            out.println(uploadfile.getBytes().length);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * kontroler wywoluje serwis zapisujacy samochod do listy ulubionych
     */
    @RequestMapping(value="/addToFavourites", method = {RequestMethod.POST, RequestMethod.GET})
    public String addToFavourites(@RequestParam(value = "carId", required = true) Long carId, @SessionAttribute("userFromSession") UserModel user) {
        CarModel car = carService.getCar(carId).getData();

        ServiceResult<UserModel> result = new ServiceResult<>();

        try {
            result = userService.addCarToFavouriteList(user, car);
        } catch (Exception e){
            log.error("Blad podczas dodawania samochodu do listy ulubionych");
            result.errors.add("Blad podczas dodawania samochodu do listy ulubionych");
        }

        // TODO: zmieniac serduszko na liscie samochodow zeby bylo widac efekt dodawania do ulubionych
        // TODO: dodac widok - lista ulubionych samochodow
        // TODO: nie zapisywac dubli do bazy

        return "redirect:/car/carsList";
    }

    @RequestMapping(value = "/carDetails", method = RequestMethod.GET)
    public String showCarDetails(Model model){

        log.info("Wyswietl szczegoly samochodu");

        return "carDetails";
    }



    /**
     * kontroler wyswietla liste ulubionych samochodow
     */
    @RequestMapping(value = "/carsList/favouriteCars", method = RequestMethod.GET)
    public String showFavouriteCarsListSite(Model model, @SessionAttribute("userFromSession") UserModel user){

        log.info("Wyswietl liste ulubionych samochodow");

        List<CarModel> carsList = user.getFavouriteCars();

        model.addAttribute("availableCarsList", carsList);
        model.addAttribute("favouritesList", true);

        return "carsList";
    }

}
