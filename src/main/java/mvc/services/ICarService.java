package mvc.services;

import mvc.helpers.ServiceResult;
import mvc.models.CarModel;

import java.util.List;

public interface ICarService {
    ServiceResult<List<CarModel>> getCarsList();
    ServiceResult<List<CarModel>> getSoldCars();
    ServiceResult<CarModel> getCar(Long carId);
}
