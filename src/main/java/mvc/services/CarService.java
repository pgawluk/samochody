package mvc.services;

import mvc.dao.ICarDAO;
import mvc.helpers.ServiceResult;
import mvc.models.CarModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("carService")
@EnableTransactionManagement
public class CarService implements ICarService {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    ICarDAO carDAO;

    public ServiceResult<List<CarModel>> getCarsList(){
        ServiceResult<List<CarModel>> result = new ServiceResult<>();

        try {
            log.info("Pobieram liste samochodow");

            // wyszukanie samochodow ktore nie sa sprzedane i nie sa dopiero zamowione (czyli sa juz w salonie)
            List<CarModel> carsList = carDAO.findAllByIsSoldIsAndIsOrderedByManagerIs(false, false);

            if(carsList == null){
                log.info("Nie znaleziono samochodow w bazie");
                carsList = new ArrayList<>(Collections.emptyList());
            }

            result.setData(carsList);

        } catch (Exception e){
            log.error("Blad podczas pobierania listy samochodow");
            result.errors.add("Nie udało się pobrać listy samochodów");
        }

        return result;
    }

    public ServiceResult<List<CarModel>> getSoldCars(){
        ServiceResult<List<CarModel>> result = new ServiceResult<>();

        try {
            log.info("Pobieram liste samochodow");

            // wyszukanie samochodow ktore nie sa sprzedane i nie sa dopiero zamowione (czyli sa juz w salonie)
            List<CarModel> carsList = carDAO.findAllByIsSoldIsAndIsOrderedByManagerIs(true, false);

            if(carsList == null){
                log.info("Nie znaleziono samochodow w bazie");
                carsList = new ArrayList<>(Collections.emptyList());
            }

            result.setData(carsList);

        } catch (Exception e){
            log.error("Blad podczas pobierania listy samochodow");
            result.errors.add("Nie udało się pobrać listy samochodów");
        }

        return result;
    }


    public ServiceResult<CarModel> getCar(Long carId){
        ServiceResult<CarModel> result = new ServiceResult<>();

        try{
            log.info("Pobieram samochod");

            CarModel car = carDAO.findByCarId(carId);

            if(car == null){
                result.errors.add("Brak samochodu w bazie");
                return result;
            }
            result.setData(car);

        } catch (Exception e){
            log.error("Blad podczas pobierania samochodu");
            result.errors.add("Nie udało się pobrać listy samochodów");
        }

        return result;
    }

    public CarModel getCarById(Long carId){
         return carDAO.findByCarId(carId);
    }
}
