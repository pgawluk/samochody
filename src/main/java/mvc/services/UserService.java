package mvc.services;

import mvc.dao.ICarDAO;
import mvc.dao.IUserDAO;
import mvc.enums.Role;
import mvc.helpers.ServiceResult;
import mvc.models.CarModel;
import mvc.models.UserModel;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;


@Service("userService")
@EnableTransactionManagement
public class UserService implements IUserService {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    IUserDAO userDAO;

    @Autowired
    ICarDAO carDAO;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    /**
     * serwis szuka uzytkownika po emailu
     * @return
     */
    @Transactional
    public ServiceResult<UserModel> getUser(String email) {
        ServiceResult<UserModel> result = new ServiceResult<>();
        UserModel user;
        try {
            user = userDAO.findByEmail(email);
            result.setData(user);
        } catch (Exception e){
            log.error("Blad podczas wyszukiwania uzytkownika");
            result.errors.add("Błąd podczas pobierania danych użytkownika");
        }

        return result;
    }

    /**
     *
     *          WAZNE - ZEBY MOZNA BYLO ZAPISAC/EDYTOWAC ZMIANY W BAZIE, METODA W SERWISIE MUSI BYC @Transactional
     *
     * serwis tworzy nowego uzytkownika
     * @return
     */
    @Transactional
    public ServiceResult<UserModel> createUser(String email, String password, String firstname, String lastname, Role role) {
        ServiceResult<UserModel> result = new ServiceResult<>();
        Calendar timeNow = Calendar.getInstance();

        try{
            ServiceResult<UserModel> uniqueMail = this.isEmailUnique(email);  //this oznacza UserService
            if(!uniqueMail.isValid()){
                result.setErrors(uniqueMail.getErrors());
                log.info("e-mail nieunikalny");
                return result;
            }

            log.info("Zapisuje uzytkownika");

            UserModel user = new UserModel();
            user.setEmail(email);
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user.setFirstname(firstname);
            user.setLastname(lastname);
            user.setIsActive(true);
            user.setRole(role);

            userDAO.save(user);
            result.setData(user);

        } catch (Exception e) {
            log.error("Blad podczas zapisywania uzytkownika do bazy");
            result.errors.add("Błąd podczas zapisywania użytkownika do bazy danych");
        }

        return result;
    }

    /**
     * serwis edytuje uzytkownika
     * @return
     */
    @Transactional
    public ServiceResult<UserModel> editUser(UserModel user) {
        ServiceResult<UserModel> result = new ServiceResult<>();

        try{
            ServiceResult<UserModel> uniqueMail = this.isEmailUnique(user.getEmail());  //this oznacza UserService
            // jesli email istnieje juz w bazie
            if(!uniqueMail.isValid()){
                // jesli ten email nalezy do innego uzytkownika
                if (userDAO.findByEmail(user.getEmail()).getUserId() != user.getUserId()) {
                    result.setErrors(uniqueMail.getErrors());
                    log.info("e-mail nieunikalny");
                    return result;
                }
            }

            log.info("Zapisuje uzytkownika");

            // kodowanie hasla
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

            userDAO.save(user);
            result.setData(user);

        } catch (Exception e) {
            log.error("Blad podczas edytowania uzytkownika");
            result.errors.add("Błąd podczas edytowania hasła lub adresu e-mail użytkownika");
        }

        return result;
    }

    @Transactional
    public ArrayList<UserModel> getUsersList(Role role) {

        ArrayList<UserModel> result = new ArrayList<>();
        result = userDAO.findAllByRole(role);
        return result;
    }

    @Transactional
    public void deleteUser(Long id){
        userDAO.deleteByUserId(id);
    }

    @Transactional
    public ServiceResult<UserModel> getUserById(Long id){

        ServiceResult<UserModel> result = new ServiceResult<>();
        UserModel userModel;

        try{
            userModel = userDAO.findByUserId(id);
            result.setData(userModel);
        }catch(Exception e){
            result.errors.add("Błąd podczas pobierania danych użytkownika");
        }
        return result;
    }


    /**
     * serwis sprawdza czy email jest unikalny - jesli znajdzie uzytkownika w bazie to zwraca blad
     */
    public ServiceResult<UserModel> isEmailUnique(String email) {

        ServiceResult<UserModel> result = new ServiceResult<>();
        UserModel user;

        try {
            user = userDAO.findByEmail(email);
            if(user != null){
                result.errors.add("Ten adres e-mail jest już zajęty");
            }
        } catch(Exception e){
            log.error("Blad podczas sprawdzania czy email jest unikalny");
            result.errors.add("Błąd bazy danych");
        }

        return result;
    }


    @Transactional
    public ServiceResult<UserModel> addCarToFavouriteList(UserModel user, CarModel car){
        ServiceResult<UserModel> result = new ServiceResult<>();

        try {
            // cos nie dziala :/ zmienilem na EAGER fetching w bazie
            //Hibernate.initialize(user.getFavouriteCars());

            user.getFavouriteCars().add(car);
            car.getFavouriteCar_Users().add(user);

            userDAO.save(user);
            carDAO.save(car);

            result.setData(user);
        } catch(Exception e){
            log.error("Blad podczas dodawania samochodu do listy ulubionych");
            result.errors.add("Blad podczas dodawania samochodu do listy ulubionych");
        }

        return result;
    }
}
