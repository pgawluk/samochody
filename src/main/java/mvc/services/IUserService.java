package mvc.services;

import mvc.enums.Role;
import mvc.helpers.ServiceResult;
import mvc.models.CarModel;
import mvc.models.UserModel;

import java.util.ArrayList;
import java.util.List;

public interface IUserService {
    ServiceResult<UserModel> getUser(String email);
    ServiceResult<UserModel> createUser(String email, String password, String firstname, String lastname, Role role);
    ServiceResult<UserModel> editUser(UserModel user);
    ArrayList<UserModel> getUsersList(Role role);
    void deleteUser(Long id);
    ServiceResult<UserModel> getUserById(Long id);
    ServiceResult<UserModel> addCarToFavouriteList(UserModel user, CarModel car);
}
