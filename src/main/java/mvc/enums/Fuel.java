package mvc.enums;

public enum Fuel {
    PETROL("benzyna"),
    DIESEL("diesel"),
    LPG("gaz"),
    ELECTRICAL("elektryczny"),
    HYBRID("hybrydowy");

    private String fuel;

    Fuel(String fuel) {
        this.fuel = fuel;
    }

    public String getFuel(){
        return fuel;
    }
}