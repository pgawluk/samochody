package mvc.enums;

public enum Role {
    ROLE_USER("Uzytkownik"),
    ROLE_MANAGER("Kierownik"),
    ROLE_ADMIN("Administrator");

    private String role;

    Role(String role){
        this.role = role;
    }

    public String getRole(){
        return role;
    }
}
