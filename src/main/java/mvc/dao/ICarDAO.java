package mvc.dao;

import mvc.models.CarModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ICarDAO extends CrudRepository<CarModel, Long> {
    List<CarModel> findAllByIsSoldIsAndIsOrderedByManagerIs(boolean isSold, boolean isOrderedByManager);
    CarModel findByCarId(Long carId);
}
