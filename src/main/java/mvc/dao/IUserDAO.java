package mvc.dao;

import mvc.enums.Role;
import mvc.models.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface IUserDAO  extends CrudRepository<UserModel, Long> {
    UserModel findByEmail(String email);
    ArrayList<UserModel> findAllByRole(Role role);
    void deleteByUserId(Long id);
    UserModel findByUserId(Long id);
}
