/**
 *
 * Zmiana z pól int na Integer, bo jak baza pozwala na null to pozniej w kodzie moze sypac bledami:
 *
 *
 *
 A null value cannot be assigned to a primitive type, like int, long, Boolean, etc. If the database column that corresponds to the field in your object can be null, then your field should be a wrapper class, like Integer, Long, Boolean, etc.

 The danger is that your code will run fine if there are no nulls in the DB, but will fail once nulls are inserted.

 And you can always return the primitive type from the getter.
 *
 */

package mvc.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import mvc.enums.Fuel;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "Car")
public class CarModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CarId")
    private long carId;

    @Column(name = "Price")
    private Integer price;

    /**
     * marka
     */
    @Column(name = "Brand")
    @NotEmpty
    private String brand;

    @Column(name = "Model")
    private String model;

    /**
     * przebieg
     */
    @Column(name = "Mileage")
    private Integer mileage;

    /**
     * rodzaj paliwa
     */
    @Column(name = "Fuel")
    private Fuel fuel;

    /**
     * moc silnika
     */
    @Column(name = "Power")
    private Integer power;

    @Column(name = "YearOfProduction")
    private Integer yearOfProduction;

    /**
     * dodatkowy opis
     */
    @Column(name = "Description")
    private String description;

    @Column(name = "Color")
    private String color;

    /**
     * czy bezwypadkowy
     */
    @Column(name = "IsAccidentFree")
    private Boolean isAccidentFree;

    /**
     * czy ubezpieczony
     */
    @Column(name = "IsInsured")
    private Boolean isInsured;

    /**
     * rodzaj napedu (np. 4x4)
     */
    @Column(name = "DrivingGear")
    private String drivingGear;

    /**
     * czy ma klimatyzacje
     */
    @Column(name = "HasAirConditioning")
    private Boolean hasAirConditioning;

    /**
     * czy ma centralny zamek
     */
    @Column(name = "HasCentralLock")
    private Boolean hasCentralLock;

    /**
     * czy ma czujnik parkowania
     */
    @Column(name = "HasParkingSensor")
    private Boolean hasParkingSensor;

    /**
     * czy sprzedany - zeby ulatwic wyszukiwanie dla listy dostepnych/sprzedanych samochodow
     */
    @Column(name = "IsSold")
    private Boolean isSold;

    /**
     * czy zamowiony do salonu przez pracownika (jesli dobrze rozumiem punkt 4.2.3 z naszego docsa :P )
     * gdy wartosc bedzie true, to samochod niewidoczny dla uzytkownika, gdy zmieni sie na false to znaczy ze
     * samochod przyszedl juz do salonu i mozna go wyswietlic uzytkownikowi do sprzedazy
     */
    @Column(name = "IsOrderedByManager")
    private Boolean isOrderedByManager;

    /**
     * czas dostawy (bo kierownik ma dostep do terminarzu dostaw, wiec jakas data dostawy zamowionego samochodu musi byc
     */
    @Column(name = "DeliveryDate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar deliveryDate;





    /**
     * polaczenia z innymi encjami
     */

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "favouriteCars")                // EAGER - zeby nie lecial blad ze nie ma sesji bazodanowej
    @JsonIgnore
    private List<UserModel> favouriteCar_Users;

    @ManyToOne
    @JoinColumn(name = "UserId")
    private UserModel user;





    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(Integer yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean isAccidentFree() {
        return isAccidentFree;
    }

    public void setAccidentFree(Boolean accidentFree) {
        isAccidentFree = accidentFree;
    }

    public Boolean isInsured() {
        return isInsured;
    }

    public void setInsured(Boolean insured) {
        isInsured = insured;
    }

    public String getDrivingGear() {
        return drivingGear;
    }

    public void setDrivingGear(String drivingGear) {
        this.drivingGear = drivingGear;
    }

    public Boolean isHasAirConditioning() {
        return hasAirConditioning;
    }

    public void setHasAirConditioning(Boolean hasAirConditioning) {
        this.hasAirConditioning = hasAirConditioning;
    }

    public Boolean isHasCentralLock() {
        return hasCentralLock;
    }

    public void setHasCentralLock(Boolean hasCentralLock) {
        this.hasCentralLock = hasCentralLock;
    }

    public Boolean isHasParkingSensor() {
        return hasParkingSensor;
    }

    public void setHasParkingSensor(Boolean hasParkingSensor) {
        this.hasParkingSensor = hasParkingSensor;
    }

    public Boolean isSold() {
        return isSold;
    }

    public void setSold(Boolean sold) {
        isSold = sold;
    }

    public Boolean isOrderedByManager() {
        return isOrderedByManager;
    }

    public void setOrderedByManager(Boolean orderedByManager) {
        isOrderedByManager = orderedByManager;
    }

    public Calendar getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Calendar deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public List<UserModel> getFavouriteCar_Users() {
        return favouriteCar_Users;
    }

    public void setFavouriteCar_Users(List<UserModel> favouriteCar_Users) {
        this.favouriteCar_Users = favouriteCar_Users;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }


//    @Override
//    public String toString() {
//        return "CarModel{" +
//                "carId=" + carId +
//                ", price='" + price + '\'' +
//                ", brand='" + brand + '\'' +
//                ", model='" + model + '\'' +
//                ", mileage=" + mileage +
//                ", fuel=" + fuel +
//                ", power=" + power +
//                ", yearOfProduction=" + yearOfProduction +
//                ", description='" + description + '\'' +
//                ", color='" + color + '\'' +
//                ", isAccidentFree=" + isAccidentFree +
//                ", isInsured=" + isInsured +
//                ", drivingGear='" + drivingGear + '\'' +
//                ", hasAirConditioning=" + hasAirConditioning +
//                ", hasCentralLock=" + hasCentralLock +
//                ", hasParkingSensor=" + hasParkingSensor +
//                ", isSold=" + isSold +
//                ", isOrderedByManager=" + isOrderedByManager +
//                ", deliveryDate=" + deliveryDate +
//                '}';
//    }
}
