package mvc.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import mvc.enums.Role;
import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "User")
public class UserModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UserId")
    private long userId;

    @Column(name = "Email", unique = true)
    @NotEmpty(message = "Pole nie moze byc puste")
    @Email(message = "Podaj poprawny adres e-mail")
    private String email;

    @Column(name = "Password")
    @NotEmpty(message = "Pole nie moze byc puste")
    private String password;

    @Column(name = "FirstName")
    @NotEmpty(message = "Pole nie moze byc puste")
    private String firstname;

    @Column(name = "LastName")
    @NotEmpty(message = "Pole nie moze byc puste")
    private String lastname;

    @Column(name = "Role")
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Pole nie moze byc puste")
    private Role role = Role.ROLE_USER;

    @Column(name = "IsActive", columnDefinition = "tinyint(1) default 1")
    @NotNull(message = "Pole nie moze byc puste")
    private Boolean isActive = true;                    //domyslnie jest true, zeby do formularza rejestracji nie lecial null (bo w tedy z formularza do kontrolera zapisujacego do bazy tez leci null i BindingResult sie czepia ze sa bledy w formularzu





    /**
     * polaczenia z innymi encjami
     */

    // ulubione samochody
    @ManyToMany(fetch = FetchType.EAGER)                            // zeby nie lecial blad ze nie ma sesji bazodanowej
    //@Cascade(org.hibernate.annotations.CascadeType.ALL)           // przez to lecial wyjatek
    @JoinTable(
            name = "User_I_FavouriteCar",
            joinColumns = { @JoinColumn(name = "userId") },
            inverseJoinColumns = { @JoinColumn(name = "carId") }
    )
    @JsonIgnore
    private List<CarModel> favouriteCars;

    // kupione samochody
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private List<CarModel> boughtCars;





    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public List<CarModel> getFavouriteCars() {
        return favouriteCars;
    }

    public void setFavouriteCars(List<CarModel> favouriteCars) {
        this.favouriteCars = favouriteCars;
    }

    public List<CarModel> getBoughtCars() {
        return boughtCars;
    }

    public void setBoughtCars(List<CarModel> boughtCars) {
        this.boughtCars = boughtCars;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", role=" + role +
                ", isActive=" + isActive +
                '}';
    }
}
